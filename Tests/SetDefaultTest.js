var dispatcher = require('../dispatcher.js');
var json=new (require('../helper/stringBuilder'));

// ******************************************************
// test case 1
//**************************************************
var failed=false;
json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_DEFAULTS_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
            json.append('{');
                json.append('"id": "CGL_TYPE",');
                json.append('"type": "String",');
                json.append('"values": ["WRL_24_02"]');
		    json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["004"]');
            json.append('}, ');
            json.append('{');
				json.append('"id": "CGL_VCREGION",');
				json.append('"type": "String",');
				json.append('"values": ["025"]');
            json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_COUNTRY",');
				json.append('"type": "String",');
				json.append('"values": ["522"]');
            json.append('},');
            json.append('{');
				json.append('"id": "CGL_SVERSION",');
				json.append('"type": "String",');
				json.append('"values": ["001"]');
            json.append('},');
            json.append('{');
				json.append('"id": "CGL_DEFAULT_OR_FIX",');
				json.append('"type": "String",');
				json.append('"values": ["D"]');
            json.append('}');
        json.append(']'); 
    json.append('}');
json.append('}');                

var expected=[
    { id: "CBS_BCKTPOSD", value: "001" },
    { id: "CBS_LOADBSUSP", value: "001" },
    { id: "CBS_LOADUNIT", value: "027" },
    { id: "CCA_HVAC", value: "001" },
    { id: "CCA_PGKCABIN", value: "002" },
    { id: "CCA_PGKSEAT", value: "002" },
    { id: "CCA_RADIO", value: "001" },
    { id: "CCA_STEERCOL", value: "003" },
    { id: "CCA_WINDOWS", value: "001" },
    { id: "CCA_WWIPER", value: "001" },
    { id: "CCW_CNTRWADD", value: "001" },
    { id: "CDT_DRIVMODE", value: "004" },
    { id: "CDT_PGKAXLE", value: "007" },
    { id: "CDT_SLOWDRV", value: "001" },
    { id: "CDT_SPEED", value: "017" },
    { id: "CEL_DISCONN", value: "005" },
    { id: "CEL_SCKTFR13", value: "001" },
    { id: "CEL_SCKTFR14", value: "001" },
    { id: "CEL_SCKTREAR", value: "001" },
    { id: "CEN_BLOCKHT", value: "001" },
    { id: "CEN_ENGINE", value: "323" },
    { id: "CEN_THROTTLE", value: "003" },
    { id: "CFT_ADAPATT", value: "019" },
    { id: "CHI_HITCH", value: "030" },
    { id: "CHY_ADDHYCR", value: "001" },
    { id: "CHY_CPLR3", value: "002" },
    { id: "CHY_CPLRSPEC", value: "012" },
    { id: "CHY_GEARPUMP", value: "016" },
    { id: "CHY_HIGHFLOW", value: "001" },
    { id: "CHY_OIL", value: "002" },
    { id: "CHY_PGKHYDSY", value: "002" },
    { id: "CHY_RETFLOW", value: "001" },
    { id: "CHY_SINKBRAKE", value: "001" },
    { id: "CLI_BEACON", value: "001" },
    { id: "CLI_WRKLGTF", value: "006" },
    { id: "COT_APPAREA", value: "003" },
    { id: "COT_WHLHCOVR", value: "001" },
    { id: "CPT_PAINTTYPE", value: "004" },
    { id: "CRD_COMBIBAG", value: "001" },
    { id: "CRD_LIGHTING", value: "001" },
    { id: "CRD_PLTBRCKT", value: "001" },
    { id: "CRD_RHOMOLOG", value: "001" },
    { id: "CRD_SCKTRATT", value: "001" },
    { id: "CSM_CENTRLUB", value: "001" },
    { id: "CSM_GREASEPR", value: "001" },
    { id: "CSM_TOOLBOX", value: "001" },
    { id: "CSM_TOOLKIT", value: "001" },
    { id: "CTT_TIREADD", value: "001" },
    { id: "CTT_TIRES", value: "302" },
    { id: "CVS_IMMOB", value: "001" },
    { id: "CVS_TELMATIC", value: "045" },
    { id: "CWS_BACKUPW", value: "001" },
    { id: "CWS_WARNSIGN", value: "001" }

];

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 1: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length + "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 1: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37mTest Case 1: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})

// ******************************************************
// test case 2
//**************************************************

json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_DEFAULTS_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
            json.append('{');
                json.append('"id": "CGL_TYPE",');
                json.append('"type": "String",');
                json.append('"values": ["A01-01"]');
		    json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["004"]');
            json.append('}, ');
            json.append('{');
				json.append('"id": "CGL_VCREGION",');
				json.append('"type": "String",');
				json.append('"values": ["013"]');
            json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_COUNTRY",');
				json.append('"type": "String",');
				json.append('"values": ["002"]');
            json.append('},');
            json.append('{');
				json.append('"id": "CGL_SVERSION",');
				json.append('"type": "String",');
				json.append('"values": ["001"]');
            json.append('},');
            json.append('{');
				json.append('"id": "CGL_DEFAULT_OR_FIX",');
				json.append('"type": "String",');
				json.append('"values": ["D"]');
            json.append('}');
        json.append(']'); 
    json.append('}');
json.append('}');                

var expected=[
    { id: "CBS_BCKTPOSD", value: "001" },
    { id: "CBS_LOADBSUSP", value: "001" },
    { id: "CBS_LOADUNIT", value: "024" },
    { id: "CCA_CABIN", value: "003" },
    { id: "CCA_RADIO", value: "001" },
    { id: "CCA_SEAT", value: "030" },
    { id: "CCA_SUNBLIND", value: "001" },
    { id: "CCA_WINDOWS", value: "001" },
    { id: "CCW_CNTRWADD", value: "001" },
    { id: "CDT_AXLE", value: "010" },
    { id: "CDT_DRIVMODE", value: "004" },
    { id: "CDT_SLOWDRV", value: "001" },
    { id: "CDT_TRCTCTRL", value: "001" },
    { id: "CEL_DISCONN", value: "004" },
    { id: "CEL_SCKTFR13", value: "001" },
    { id: "CEL_SCKTREAR", value: "001" },
    { id: "CEN_BLOCKHT", value: "001" },
    { id: "CEN_ENGINE", value: "323" },
    { id: "CEN_THROTTLE", value: "003" },
    { id: "CFT_ADAPATT", value: "133" },
    { id: "CGL_EDITION", value: "001" },
    { id: "CHI_HITCH", value: "030" },
    { id: "CHY_ADDHYCR", value: "001" },
    { id: "CHY_CPLR3", value: "002" },
    { id: "CHY_CPLR34", value: "001" },
    { id: "CHY_LOADHYDS", value: "004" },
    { id: "CHY_OIL", value: "002" },
    { id: "CHY_RETFLOW", value: "001" },
    { id: "CHY_RETFLOWR", value: "001" },
    { id: "CHY_SINKBRAKE", value: "001" },
    { id: "CLI_BEACON", value: "001" },
    { id: "CLI_LIGHTGRD", value: "001" },
    { id: "CLI_WRKLGTF", value: "006" },
    { id: "COT_APPAREA", value: "003" },
    { id: "COT_WHLHCOVR", value: "001" },
    { id: "CPT_PAINTTYPE", value: "004" },
    { id: "CRD_COMBIBAG", value: "001" },
    { id: "CRD_COMBIBOX", value: "001" },
    { id: "CRD_LIGHTING", value: "001" },
    { id: "CRD_MUDFLAP", value: "001" },
    { id: "CRD_PLTBRCKT", value: "001" },
    { id: "CRD_RHOMOLOG", value: "001" },
    { id: "CRD_SCKTRATT", value: "001" },
    { id: "CSM_CENTRLUB", value: "001" },
    { id: "CSM_GREASEPR", value: "001" },
    { id: "CSM_SECPACK", value: "001" },
    { id: "CSM_TOOLBOX", value: "001" },
    { id: "CSM_TOOLKIT", value: "001" },
    { id: "CTT_TIREADD", value: "001" },
    { id: "CTT_TIRES", value: "301" },
    { id: "CTT_TIRESPEC", value: "001" },
    { id: "CVS_IMMOB", value: "001" },
    { id: "CVS_TELMATIC", value: "001" },
    { id: "CWS_BACKUPW", value: "001" },
    { id: "CWS_WARNSIGN", value: "001" }
];

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 2: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length + "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 2: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37mTest Case 2: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})

// ******************************************************
// test case 3
//**************************************************

json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_DEFAULTS_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
            json.append('{');
                json.append('"id": "CGL_TYPE",');
                json.append('"type": "String",');
                json.append('"values": ["DUMP_D26-01"]');
		    json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["004"]');
            json.append('}, ');
            json.append('{');
				json.append('"id": "CGL_VCREGION",');
				json.append('"type": "String",');
				json.append('"values": ["025"]');
            json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_COUNTRY",');
				json.append('"type": "String",');
				json.append('"values": ["504"]');
            json.append('},');
            json.append('{');
				json.append('"id": "CGL_SVERSION",');
				json.append('"type": "String",');
				json.append('"values": ["002"]');
            json.append('},');
            json.append('{');
				json.append('"id": "CGL_DEFAULT_OR_FIX",');
				json.append('"type": "String",');
				json.append('"values": ["D"]');
            json.append('}');
        json.append(']'); 
    json.append('}');
json.append('}');                

var expected=[
    { id: "CBS_SKIP", value: "009" },
    { id: "CCA_CABIN", value: "006" },
    { id: "CCA_DOCMENTS", value: "001" },
    { id: "CCA_HVAC", value: "001" },
    { id: "CCA_RADIO", value: "001" },
    { id: "CDT_SPEED", value: "022" },
    { id: "CEN_AUTOSHUT", value: "002" },
    { id: "CEN_ENGINE", value: "031" },
    { id: "CGL_CE", value: "002" },
    { id: "CGL_EDITION", value: "001" },
    { id: "CGL_EXPORT", value: "001" },
    { id: "CHI_TOWATT", value: "001" },
    { id: "CHY_OIL", value: "004" },
    { id: "CLI_BEACON", value: "010" },
    { id: "CLI_MARKLGT", value: "001" },
    { id: "CLI_WRKLGTF", value: "001" },
    { id: "CLI_WRKLGTFR", value: "001" },
    { id: "CLI_WRKLGTR", value: "001" },
    { id: "COT_FULLTANK", value: "001" },
    { id: "COT_IDPLATE", value: "001" },
    { id: "COT_MANAPPR", value: "001" },
    { id: "COT_PRESERV", value: "001" },
    { id: "COT_ZEPID", value: "001" },
    { id: "CPT_DECAL", value: "005" },
    { id: "CPT_DECALSLN", value: "002" },
    { id: "CPT_PAINTTYPE", value: "003" },
    { id: "CRD_PLTBRCKT", value: "001" },
    { id: "CRD_RHOMOLOG", value: "001" },
    { id: "CRD_ROADEQ", value: "002" },
    { id: "CSM_SECPACK", value: "001" },
    { id: "CTT_TIRES", value: "364" },
    { id: "CVS_IMMOB", value: "001" },
    { id: "CVS_TELMATIC", value: "001" },
    { id: "CWS_ADDCAM", value: "001" },
    { id: "CWS_BACKUPC", value: "001" },
    { id: "CWS_BELT", value: "003" },
    { id: "CWS_FRNTGRD", value: "001" },
    { id: "CWS_FRONTCAM", value: "002" },
    { id: "CWS_REFLECTR", value: "001" },
    { id: "CWS_ROPSLOCK", value: "001" },
    { id: "CWS_SAFEDCL", value: "001" },
    { id: "CWS_SEATSW", value: "001" },
    { id: "CWS_STPFLOU", value: "001" },
    { id: "CWS_STPSIGN", value: "001" }
    
];

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 3: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length + "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 3: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37mTest Case 3: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})

//console.log(json.toString())

console.log(result.status == 200 && failed == false ? "\x1b[37mSetEdition Test: \x1b[32msuccessful\x1b[0m" : "SetEdition Test: \x1b[31mfailed\x1b[0m" )