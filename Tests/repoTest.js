'use strict';

const { Console } = require('console');

try {
    const PropertiesReader = require('properties-reader');
    const prop = PropertiesReader('config/app.properties');
    var repo = new (require('../helper/repo'));
    const { PerformanceObserver, performance } = require('perf_hooks');
    var util = require('util');
    var sql = 'select * from ' + repo.getSchema() + 'ZVC_BASIC_CFG;'
    var t0 = performance.now()
    var connection = repo.getConnection();
    connection.exec(sql);
    //console.log(util.inspect(result, { colors: false }));
    var t1 = performance.now();
    //console.log("time in ms " +  (t1 - t0));
    connection.disconnect();
    console.log('Repo Test: \x1b[32msuccessful\x1b[0m')
}
catch (e) {
    console.log("Repo Test: \x1b[31mfailed\x1b[0m")
}
