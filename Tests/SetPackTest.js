var failure=false;
var dispatcher = require('../dispatcher.js');
var json=new (require('../helper/stringBuilder'));
var failed=false;

//******************************************************
// Test Case 1
//*****************************************************
json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_PACK_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
            json.append('{');
                json.append('"id": "CGL_TYPE",');
                json.append('"type": "String",');
                json.append('"values": ["A01-01"]');
		    json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["007"]');
            json.append('}, ');
            json.append('{');
				json.append('"id": "CGL_PACK_01",');
				json.append('"type": "String",');
				json.append('"values": ["001"]');
            json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_PACK_02",');
				json.append('"type": "String",');
				json.append('"values": ["002"]');
            json.append('},');
            json.append('{');
                json.append('"id": "CGL_PACK_03",');
                json.append('"type": "String",');
                json.append('"values": ["003"]');
            json.append('},'); 
            json.append('{');
                json.append('"id": "CGL_PACK_04",');
                json.append('"type": "String",');
                json.append('"values": ["004"]');
            json.append('},'); 
            json.append('{');
                json.append('"id": "CGL_PACK_05",');
                json.append('"type": "String",');
                json.append('"values": ["005"]');
            json.append('}'); 
        json.append(']'); 
    json.append('}');
json.append('}');     

var expected=[  
    { id: "CCA_CABIN", value: "004" },
    { id: "CCA_RADIO", value: "003" }
]

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 1: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length + "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 1: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37mTest Case 1: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})

//******************************************************
// Test Case 2
//*****************************************************
json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_PACK_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
            json.append('{');
                json.append('"id": "CGL_TYPE",');
                json.append('"type": "String",');
                json.append('"values": ["DUMP_D26-01"]');
		    json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["*"]');
            json.append('}, ');
            json.append('{');
				json.append('"id": "CGL_PACK_01",');
				json.append('"type": "String",');
				json.append('"values": ["001"]');
            json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_PACK_02",');
				json.append('"type": "String",');
				json.append('"values": ["002"]');
            json.append('},');
            json.append('{');
                json.append('"id": "CGL_PACK_03",');
                json.append('"type": "String",');
                json.append('"values": ["003"]');
            json.append('},'); 
            json.append('{');
                json.append('"id": "CGL_PACK_04",');
                json.append('"type": "String",');
                json.append('"values": ["004"]');
            json.append('},'); 
            json.append('{');
                json.append('"id": "CGL_PACK_05",');
                json.append('"type": "String",');
                json.append('"values": ["005"]');
            json.append('}'); 
        json.append(']'); 
    json.append('}');
json.append('}');     

var expected=[
    { id: "CBS_SKIP", value: "003" },
    { id: "CCA_CABIN", value: "014" },
    { id: "CCA_HVAC", value: "002" }
]

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 2: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length + "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 2: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37mTest Case 2: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})
console.log(result.status == 200 && failed == false ? "\x1b[37mSetPack Test: \x1b[32msuccessful\x1b[0m" : "SetEdition Test: \x1b[31mfailed\x1b[0m" )