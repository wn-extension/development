var dispatcher = require('../dispatcher.js');
var json=new (require('../helper/stringBuilder'));

// ******************************************************
// test case 1
//******************************************************
json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_EDITION_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
        json.append('{');
            json.append('"id": "CGL_TYPE",');
            json.append('"type": "String",');
            json.append('"values": ["DUMP_D26-01"]');
		json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["004"]');
            json.append('}, ');
            json.append('{');
				json.append('"id": "CGL_VCREGION",');
				json.append('"type": "String",');
				json.append('"values": ["025"]');
            json.append('},'); 
            json.append('{');
                json.append('"id": "CGL_EDITION",');
                json.append('"type": "String",');
                json.append('"values": ["003"]');
            json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_COUNTRY",');
				json.append('"type": "String",');
				json.append('"values": ["522"]');
            json.append('}');
            json.append(']'); 
    json.append('}');
json.append('}');            

var expected=[
    { id: "CBS_SKIP", value: "031" },
    { id: "CCA_CABIN", value: "006" },
    { id: "CEL_SCKTTOW", value: "002" },
    { id: "CEN_ENGINE", value: "032" },
    { id: "CHI_TOWATT", value: "002" },
    { id: "CLI_MARKLGT", value: "002" },
    { id: "CRD_PLTBRCKT", value: "002" },
    { id: "CRD_ROADEQ", value: "002" },
    { id: "CWS_BELT", value: "005" },
    { id: "CWS_SEATSW", value: "002" },
    { id: "CWS_STPSIGN", value: "002" }
];

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));

var failed=false
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 1: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length + "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 1: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37mTest Case 1: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})

// ******************************************************
// test case 2
//******************************************************
json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_EDITION_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
        json.append('{');
            json.append('"id": "CGL_TYPE",');
            json.append('"type": "String",');
            json.append('"values": ["A01-01"]');
		json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["007"]');
            json.append('}, ');
            json.append('{');
                json.append('"id": "CGL_EDITION",');
                json.append('"type": "String",');
                json.append('"values": ["002"]');
            json.append('}'); 
            json.append(']'); 
    json.append('}');
json.append('}');            

expected=[
    { id: "CCA_WINDOWS", value: "005" }
];

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 2: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length+ "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 2: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37m" + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})

// ******************************************************
// test case 3
//******************************************************

json.clear();
json.append("{");
    json.append('"type": "vfun",');
    json.append('"vfunInput": {');
        json.append('"id": "ZVC_SET_EDITION_2",');
        json.append('"kbHeaderInfo": {');
            json.append('"id": 4711,');
            json.append('"key": {');
                json.append('"logsys": "R7ECLNT800",');
                json.append('"kbName": "000000001000436234",');
                json.append('"kbVersion": "5.0"');
            json.append('},');
            json.append('"validFromDate": "2021-10-04",'); 
            json.append('"changeDate": "2021-10-04",');
            json.append('"build": 2,');
            json.append('"structureHash": "9487B6703B211D9853BF522D838BD26B"');
        json.append('},');
        json.append('"fnArgs": [')
        json.append('{');
            json.append('"id": "CGL_TYPE",');
            json.append('"type": "String",');
            json.append('"values": ["RPBA_E19-01"]');
		json.append('},'); 
            json.append('{');
				json.append('"id": "CGL_MODEL",');
				json.append('"type": "String",');
				json.append('"values": ["004"]');
            json.append('}, ');
            json.append('{');
                json.append('"id": "CGL_EDITION",');
                json.append('"type": "String",');
                json.append('"values": ["004"]');
            json.append('}'); 
            json.append(']'); 
    json.append('}');
json.append('}');            

expected=[
    { id: "CBS_PISTRODP", value: "002" },
    { id: "CCA_CABIN", value: "014" },
    { id: "CCA_HVAC", value: "003" },
    { id: "CCA_RADIO", value: "006" },
    { id: "CCW_CNTRWGHT", value: "002" },
    { id: "CEL_REFLPUMP", value: "002" },
    { id: "CEN_ENGINE", value: "138" },
    { id: "CFT_PREP", value: "004" },
    { id: "CHY_3RDHYC", value: "021" },
    { id: "CHY_HYCGRIP", value: "002" },
    { id: "CHY_PRELEASE", value: "013" },
    { id: "CHY_SHKREL", value: "002" },
    { id: "CLI_WRKLGTFR", value: "002" },
    { id: "CWS_OLOADWRN", value: "005" }

];

var result=dispatcher.functionDispatcher(JSON.parse(json.toString()));
if (result.data.vfunOutput.fnArgs.length != expected.length) {
    console.log("\x1b[37mTest Case 3: Different count of rows! Expected: " + expected.length + " received: \x1b[31m" + result.data.vfunOutput.fnArgs.length + "\x1b[0m");
    failed=true;
}
expected.forEach( item => {
    var current = result.data.vfunOutput.fnArgs.find(({ id }) => id === item.id);
    if (current == null) {
        failed = true;
        console.log("\x1b[37mTest Case 3: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m not found \x1b[0m");
    } else
    if ( current.values[0]!= item.value) {
        failed = true;
        console.log("\x1b[37mTest Case 3: " + item.id + " -> " + "expected: " + item.value + " received: \x1b[31m" + current.values[0] + "\x1b[0m");
    }
})

console.log(result.status == 200 && failed == false ? "\x1b[37mSetEdition Test: \x1b[32msuccessful\x1b[0m" : "SetEdition Test: \x1b[31mfailed\x1b[0m" )