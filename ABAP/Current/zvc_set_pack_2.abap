FUNCTION zvc_set_pack_2 .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(GLOBALS) LIKE  CUOV_00 STRUCTURE  CUOV_00
*"  TABLES
*"      QUERY STRUCTURE  CUOV_01
*"      MATCH STRUCTURE  CUOV_01
*"  EXCEPTIONS
*"      FAIL
*"      INTERNAL_ERROR
*"----------------------------------------------------------------------


* P. Muthsam, SAP, 08.02.2019:
* Erweiterung für CGL_MODEL


  CONSTANTS: lc_atnam_type        TYPE atnam VALUE 'CGL_TYPE',
             lc_atnam_model       TYPE atnam VALUE 'CGL_MODEL',
*             lc_atnam_pack        TYPE atnam VALUE 'CGL_PACK',
             lc_atnam_pack_prefix TYPE atnam VALUE 'CGL_PACK'.

  TYPES: BEGIN OF st_pack_vals,
           type    TYPE zvc_pack-type,
           atnam   TYPE zvc_pack-atnam,
           atwrt   TYPE zvc_pack-atwrt,
           model   TYPE zvc_pack-model,
           pack_id TYPE zvc_pack-pack_id,
           datuv   TYPE zvc_pack-datuv,
           datub   TYPE zvc_pack-datub,
         END OF st_pack_vals.

  DATA: lv_type              TYPE atwrt,
        lv_model             TYPE atwrt,
        lv_pack_id           TYPE atwrt,
        lv_atnam_pack        TYPE atwrt,
        lv_atnam_pack_wc     TYPE atwrt,
        lv_pack_counter_n(2) TYPE n,
        lv_pack_counter      TYPE i,
        lt_pack_ids          TYPE STANDARD TABLE OF atwrt,
        ls_val               TYPE cudbt_val,
        lt_vallist           TYPE cudbt_t_inst_char_vallist,
        ls_vallist           LIKE LINE OF lt_vallist,       "#EC NEEDED
        ls_value             TYPE cudbt_s_char_val,         "#EC NEEDED
        lt_pack_vals         TYPE TABLE OF st_pack_vals,
        lt_pack_vals_2       TYPE TABLE OF st_pack_vals,
        lt_pack_vals_del     TYPE TABLE OF st_pack_vals,
        lv_lines             TYPE i,
        lv_pack_vals_atwrt   TYPE atwrt.

  FIELD-SYMBOLS: <fs_pack_vals>   TYPE st_pack_vals,
                 <fs_pack_vals_2> TYPE st_pack_vals,
                 <fs_query>       TYPE cuov_01.



********************************************
* Merkmale ermitteln
********************************************
* Type
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_type
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_type
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* Model
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_model
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_model
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* Package IDs
  lv_pack_counter = 1.
  CONCATENATE lc_atnam_pack_prefix '*' INTO lv_atnam_pack_wc.

  LOOP AT query ASSIGNING <fs_query>
                WHERE varnam CP lv_atnam_pack_wc.

    lv_pack_counter_n = lv_pack_counter.
    CONCATENATE lc_atnam_pack_prefix '_' lv_pack_counter_n INTO lv_atnam_pack.

    CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
      EXPORTING
        argument      = lv_atnam_pack
      IMPORTING
*       vtype         = 'CHAR'
        sym_val       = lv_pack_id
*       NUM_VAL       =
*       IO_FLAG       =
      TABLES
        query         = query
      EXCEPTIONS
        arg_not_found = 1
        OTHERS        = 2.

    IF sy-subrc = 0.
      APPEND lv_pack_id TO lt_pack_ids.
      ADD 1 TO lv_pack_counter.
* Last PACK cstic reached in previous loop -> exit
    ELSE.
      EXIT.
    ENDIF.

  ENDLOOP.


* Peter Muthsam, 20.12.2021:
* Add loop over all package cstics (passed with CGL_PACK_xx) into variant function.
* Advantage: Variant function must be called only once.

  LOOP AT lt_pack_ids ASSIGNING FIELD-SYMBOL(<fs_pack_id>).

    lv_pack_id = <fs_pack_id>.

********************************************
* Alle Werte für Paket auslesen
********************************************

* Spezifische Defaultwerte für Type holen
    SELECT * FROM zvc_pack INTO CORRESPONDING FIELDS OF TABLE lt_pack_vals
      WHERE type = lv_type AND
            ( model = lv_model OR
              model = '*' ) AND
            pack_id = lv_pack_id AND
            datuv <= globals-date AND
            datub > globals-date
      ORDER BY atnam.


********************************************
* Relevanten Eintrag pro Merkmalswert ermitteln
********************************************

* Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
* Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
    LOOP AT lt_pack_vals ASSIGNING <fs_pack_vals>.

      AT NEW atnam.

* Hilfstabelle lt_default_vals_2 aufbauen; diese enthält die Einträge für das aktuelle ATNAM
        LOOP AT lt_pack_vals ASSIGNING <fs_pack_vals_2>
                             WHERE atnam = <fs_pack_vals>-atnam.
          APPEND <fs_pack_vals_2> TO lt_pack_vals_2.
        ENDLOOP.

* Wie lang ist diese Hilfstabelle? 1 oder 2 Einträge?
        DESCRIBE TABLE lt_pack_vals_2 LINES lv_lines.

* Hilfstabelle nach Zählen der Einträge wieder leeren.
        REFRESH lt_pack_vals_2.

* 2 Einträge => Für dieses ATNAM gibt es Eintrag mit spezifischem Modell und mit * => Den allgemeinen Eintrag
* (Modell = *) aus der ursprünglichen int. Tabelle löschen
        IF lv_lines = 2.

          LOOP AT lt_pack_vals ASSIGNING <fs_pack_vals_2>
                               WHERE model = '*' AND
                                     atnam = <fs_pack_vals>-atnam.

            APPEND <fs_pack_vals_2> TO lt_pack_vals_del.

          ENDLOOP.

        ENDIF.

      ENDAT.

    ENDLOOP.

*Tabelleneinträge löschen, die zuvor ausgeschlossen wurden.
    LOOP AT lt_pack_vals_del ASSIGNING <fs_pack_vals>.
      DELETE TABLE lt_pack_vals FROM <fs_pack_vals>.
    ENDLOOP.


********************************************
* Paket-werte setzen
********************************************
    LOOP AT lt_pack_vals ASSIGNING <fs_pack_vals>.

* Peter Muthsam, SAP, 20.12.2021:
* Change from pFunction to Function call (no call of CUPR function), due to the limitation in CPS (does not allow
* pFunctions).

*    CLEAR ls_val.
*    ls_val-atfor = 'CHAR'.
*    ls_val-atwrt = <fs_pack_vals>-atwrt.
*
*    CALL FUNCTION 'CUPR_SET_VAL'
*      EXPORTING
*        instance               = globals-self
*        characteristic         = <fs_pack_vals>-atnam
*        val                    = ls_val
*      EXCEPTIONS
*        unknown_instance       = 1
*        unknown_characteristic = 2
*        internal_error         = 3
*        OTHERS                 = 4.

      lv_pack_vals_atwrt = <fs_pack_vals>-atwrt.

      CALL FUNCTION 'CUOV_SET_FUNCTION_ARGUMENT'
        EXPORTING
          argument                = <fs_pack_vals>-atnam
          vtype                   = 'CHAR'
          sym_val                 = lv_pack_vals_atwrt
*         NUM_VAL                 =
        TABLES
          match                   = match
        EXCEPTIONS
          existing_value_replaced = 1
          OTHERS                  = 2.

*   Falls nicht vorhandes Merkmal in der Tabelle gepflegt, nicht aussteigen, sondern Fehler ignorieren
      IF sy-subrc <> 0.
        CONTINUE.
      ENDIF.

    ENDLOOP.      " AT lt_pack_vals

  ENDLOOP.      " AT lt_pack_ids


ENDFUNCTION.