FUNCTION zvc_set_edition_2 .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(GLOBALS) LIKE  CUOV_00 STRUCTURE  CUOV_00
*"  TABLES
*"      QUERY STRUCTURE  CUOV_01
*"      MATCH STRUCTURE  CUOV_01
*"  EXCEPTIONS
*"      FAIL
*"      INTERNAL_ERROR
*"----------------------------------------------------------------------


* P. Muthsam, SAP, 08.02.2019:
* Erweiterung für CGL_MODEL


  CONSTANTS: lc_atnam_type    TYPE atnam VALUE 'CGL_TYPE',
             lc_atnam_model   TYPE atnam VALUE 'CGL_MODEL',
             lc_atnam_edition TYPE atnam VALUE 'CGL_EDITION'.

  TYPES: BEGIN OF st_edition_vals,
           type       TYPE zvc_edition-type,
           atnam      TYPE zvc_edition-atnam,
           atwrt      TYPE zvc_edition-atwrt,
           model      TYPE zvc_edition-model,
           edition_id TYPE zvc_edition-edition_id,
           datuv      TYPE zvc_edition-datuv,
           datub      TYPE zvc_edition-datub,
         END OF st_edition_vals.

  DATA: lv_type               TYPE atwrt,
        lv_model              TYPE atwrt,
        lv_edition_id         TYPE atwrt,
        ls_val                TYPE cudbt_val,
        lt_vallist            TYPE cudbt_t_inst_char_vallist,
        ls_vallist            LIKE LINE OF lt_vallist,      "#EC NEEDED
        ls_value              TYPE cudbt_s_char_val,        "#EC NEEDED
        lt_edition_vals       TYPE TABLE OF st_edition_vals,
        lt_edition_vals_2     TYPE TABLE OF st_edition_vals,
        lt_edition_vals_del   TYPE TABLE OF st_edition_vals,
        lv_lines              TYPE i,
        lv_edition_vals_atwrt TYPE atwrt.

  FIELD-SYMBOLS: <fs_edition_vals>   TYPE st_edition_vals,
                 <fs_edition_vals_2> TYPE st_edition_vals.



********************************************
* Merkmale ermitteln
********************************************
* Type
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_type
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_type
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* Model
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_model
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_model
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* Package ID
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_edition
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_edition_id
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.

********************************************
* Alle Werte für Paket auslesen
********************************************

* Spezifische Defaultwerte für Type holen
  SELECT * FROM zvc_edition INTO CORRESPONDING FIELDS OF TABLE lt_edition_vals
    WHERE type = lv_type AND
          ( model = lv_model OR
            model = '*' ) AND
          edition_id = lv_edition_id AND
          datuv <= globals-date AND
          datub > globals-date
    ORDER BY atnam.


********************************************
* Relevanten Eintrag pro Merkmalswert ermitteln
********************************************

* Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
* Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
  LOOP AT lt_edition_vals ASSIGNING <fs_edition_vals>.

    AT NEW atnam.

* Hilfstabelle lt_default_vals_2 aufbauen; diese enthält die Einträge für das aktuelle ATNAM
      LOOP AT lt_edition_vals ASSIGNING <fs_edition_vals_2>
                           WHERE atnam = <fs_edition_vals>-atnam.
        APPEND <fs_edition_vals_2> TO lt_edition_vals_2.
      ENDLOOP.

* Wie lang ist diese Hilfstabelle? 1 oder 2 Einträge?
      DESCRIBE TABLE lt_edition_vals_2 LINES lv_lines.

* Hilfstabelle nach Zählen der Einträge wieder leeren.
      REFRESH lt_edition_vals_2.

* 2 Einträge => Für dieses ATNAM gibt es Eintrag mit spezifischem Modell und mit * => Den allgemeinen Eintrag
* (Modell = *) aus der ursprünglichen int. Tabelle löschen
      IF lv_lines = 2.

        LOOP AT lt_edition_vals ASSIGNING <fs_edition_vals_2>
                             WHERE model = '*' AND
                                   atnam = <fs_edition_vals>-atnam.

          APPEND <fs_edition_vals_2> TO lt_edition_vals_del.

        ENDLOOP.

      ENDIF.

    ENDAT.

  ENDLOOP.

*Tabelleneinträge löschen, die zuvor ausgeschlossen wurden.
  LOOP AT lt_edition_vals_del ASSIGNING <fs_edition_vals>.
    DELETE TABLE lt_edition_vals FROM <fs_edition_vals>.
  ENDLOOP.


********************************************
* Paket-werte setzen
********************************************
  LOOP AT lt_edition_vals ASSIGNING <fs_edition_vals>.

* Peter Muthsam, SAP, 20.12.2021:
* Change from pFunction to Function call (no call of CUPR function), due to the limitation in CPS (does not allow
* pFunctions).

*    CLEAR ls_val.
*    ls_val-atfor = 'CHAR'.
*    ls_val-atwrt = <fs_edition_vals>-atwrt.
*
*    CALL FUNCTION 'CUPR_SET_VAL'
*      EXPORTING
*        instance               = globals-self
*        characteristic         = <fs_edition_vals>-atnam
*        val                    = ls_val
*      EXCEPTIONS
*        unknown_instance       = 1
*        unknown_characteristic = 2
*        internal_error         = 3
*        OTHERS                 = 4.

    lv_edition_vals_atwrt = <fs_edition_vals>-atwrt.

    CALL FUNCTION 'CUOV_SET_FUNCTION_ARGUMENT'
      EXPORTING
        argument                = <fs_edition_vals>-atnam
        vtype                   = 'CHAR'
        sym_val                 = lv_edition_vals_atwrt
*       NUM_VAL                 =
      TABLES
        match                   = match
      EXCEPTIONS
        existing_value_replaced = 1
        OTHERS                  = 2.

*   Falls nicht vorhandes Merkmal in der Tabelle gepflegt, nicht aussteigen, sondern Fehler ignorieren
    IF sy-subrc <> 0.
      CONTINUE.
    ENDIF.

  ENDLOOP.


ENDFUNCTION.