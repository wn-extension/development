FUNCTION zvc_set_defaults_or_fix .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(GLOBALS) LIKE  CUOV_00 STRUCTURE  CUOV_00
*"  TABLES
*"      QUERY STRUCTURE  CUOV_01
*"      MATCH STRUCTURE  CUOV_01
*"  EXCEPTIONS
*"      FAIL
*"      INTERNAL_ERROR
*"----------------------------------------------------------------------

  CONSTANTS: lc_atnam_type     TYPE atnam VALUE 'CGL_TYPE',
             lc_atnam_model    TYPE atnam VALUE 'CGL_MODEL',
             lc_atnam_vcregion TYPE atnam VALUE 'CGL_VCREGION',
             lc_atnam_country  TYPE atnam VALUE 'CGL_COUNTRY',
             lc_atnam_sversion TYPE atnam VALUE 'CGL_SVERSION',
             lc_atnam_d_or_f   TYPE atnam VALUE 'CGL_DEFAULT_OR_FIX'.

  TYPES: BEGIN OF st_default_vals,
           type   TYPE zvc_basic_cfg-type,
           atnam  TYPE zvc_basic_cfg-atnam,
           atwrt  TYPE zvc_basic_cfg-atwrt,
           model  TYPE zvc_basic_cfg-model,
           action TYPE zvc_basic_cfg-action,
           datuv  TYPE zvc_basic_cfg-datuv,
           datub  TYPE zvc_basic_cfg-datub,
         END OF st_default_vals.


  DATA: lv_defaults_userparm TYPE c,
        lv_type              TYPE atwrt,
        lv_model             TYPE atwrt,
        lv_region            TYPE atwrt,
        lv_country           TYPE atwrt,
        lv_sversion          TYPE atwrt,
        lv_d_or_f            TYPE atwrt,
        lv_atinn             TYPE atinn,
        lv_sysubrc           TYPE subrc,
        lt_domain            TYPE STANDARD TABLE OF ddb_c03,
        lr_action            TYPE RANGE OF zvc_action,
        ls_action            LIKE LINE OF  lr_action,
        ls_val               TYPE cudbt_val,
        lt_vallist           TYPE cudbt_t_inst_char_vallist,
        ls_vallist           LIKE LINE OF lt_vallist,
        ls_value             TYPE cudbt_s_char_val,
        lt_default_vals      TYPE TABLE OF st_default_vals,
        lt_default_vals_2    TYPE TABLE OF st_default_vals,
        lt_default_vals_del  TYPE TABLE OF st_default_vals,
        lt_basic_cfg         TYPE TABLE OF st_default_vals WITH NON-UNIQUE SORTED KEY action COMPONENTS action,
        lt_region_cfg        TYPE TABLE OF st_default_vals WITH NON-UNIQUE SORTED KEY action COMPONENTS action,
        lt_region_cfg_def    TYPE TABLE OF st_default_vals,
        lt_region_cfg_2      TYPE TABLE OF st_default_vals,
        lt_region_cfg_del    TYPE TABLE OF st_default_vals,
        lt_country_cfg       TYPE TABLE OF st_default_vals WITH NON-UNIQUE SORTED KEY action COMPONENTS action,
        lt_country_cfg_def   TYPE TABLE OF st_default_vals,
        lt_country_cfg_2     TYPE TABLE OF st_default_vals,
        lt_country_cfg_del   TYPE TABLE OF st_default_vals,
        lt_special_cfg       TYPE TABLE OF st_default_vals WITH NON-UNIQUE SORTED KEY action COMPONENTS action,
        lt_special_cfg_def   TYPE TABLE OF st_default_vals,
        lt_special_cfg_2     TYPE TABLE OF st_default_vals,
        lt_special_cfg_del   TYPE TABLE OF st_default_vals,
        lv_lines             TYPE i.
*        lt_default_cstics    TYPE STANDARD TABLE OF atnam.

  FIELD-SYMBOLS: <fs_default_vals>   TYPE st_default_vals,
                 <fs_default_vals_2> TYPE st_default_vals,
                 <fs_region_cfg>     TYPE st_default_vals,
                 <fs_region_cfg_2>   TYPE st_default_vals,
                 <fs_country_cfg>    TYPE st_default_vals,
                 <fs_country_cfg_2>  TYPE st_default_vals,
                 <fs_special_cfg>    TYPE st_default_vals,
                 <fs_special_cfg_2>  TYPE st_default_vals.


* P. Muthsam, 05.01.2018
* Das Setzen der Defaultwerte soll durch VC-Modellierer deaktivierbar sein.
* Hintergrund: Bei der Einführung der neuen VC-Modellierung mit:
* - Definition der Defaultwerte in Tabellen ZVC_BASIC_CFG, ZVC_REGION_CFG, ZVC_COUNTRY_CFG, ZVC_SPECIAL_CFG
*   (Setzen der Defaultwerte über diese Variantenfunktion ZVC_SET_DEFAULTS)
* - Beschreibung der gegenseitigen Abhängigkeiten über Constraints (z.B. CS_EXCV_CABIN_HVAC)
* zeigte sich, dass bei Setzen von zu viel Defaultwerten (auf "beiden Seiten" von Abhängigkeiten,
* z.B. für Kabine und Heizung) keine alternativen Werte mehr auswählbar waren. Grund: Durch das Setzen
* des Defaultwertes auf jeder Seite der Abhängigkeit setzten Constraints für die andere Seite der
* Anhängigkeit den einzigen erlaubten Wert.
* Lösung: Sparsameres Setzen von Defaultwerten (nur auf einer Seite der Abhängigkeit).
*
* Daher wird der User-Parameter ZVC_SET_DEFAULTS eingeführt. Wenn dieser auf 'F' (oder 'N') gesetzt
* ist (SU3), dann werden keine Defaultwerte gesetzt.
*
* P. Muthsam, 14.01.2019
* Die 4 Tabellen mit Default-Werten (ZVC_BASIC_CFG, ZVC_REGION_CFG, ZVC_COUNTRY_CFG, ZVC_SPECIAL_CFG) wurden ergänzt
* um das Feld Model. Die Auswerte-Logik musste entsprechende erweitert werden. Dabei ist zu beachten, dass für MODEL
* der Wert * in den Tabellen eingetragen werden kann. Dieser Eintrag gilt dann für alle Modelle - außer für die, für
* die spezifischer Eintrag vorhanden ist.
* Außerdem wurde die Logik für das Löschen und (Neu-)Setzen von Vorschlagswerten verbessert. Es werden nun nur noch
* die Defaultwerte gelöscht, die durch das Update keinen Wert mehr haben (also für die kein neuer und anderer
* Defaultwert gesetzt wird).



  GET PARAMETER ID 'ZVC_SET_DEFAULTS' FIELD lv_defaults_userparm.

  IF lv_defaults_userparm = 'F' OR
     lv_defaults_userparm = 'N'.

    EXIT.

  ENDIF.

********************************************
* Übergegebene Merkmalswerte ermitteln
********************************************
* Type
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_type
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_type
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* Model
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_model
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_model
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* Region
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_vcregion
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_region
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* Bestimmungsland
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_country
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_country
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* S-Version
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_sversion
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_sversion
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    RAISE internal_error.
  ENDIF.


* DEFAULT OR FIX
  CALL FUNCTION 'CUOV_GET_FUNCTION_ARGUMENT'
    EXPORTING
      argument      = lc_atnam_d_or_f
    IMPORTING
*     vtype         = 'CHAR'
      sym_val       = lv_d_or_f
*     NUM_VAL       =
*     IO_FLAG       =
    TABLES
      query         = query
    EXCEPTIONS
      arg_not_found = 1
      OTHERS        = 2.

  IF sy-subrc = 0.

    ls_action-sign = 'I'.
    ls_action-option = 'EQ'.

    CASE lv_d_or_f.
      WHEN 'F' OR 'D'.
        ls_action-low = lv_d_or_f.
        APPEND ls_action TO lr_action.
      WHEN 'B'.
        ls_action-low = 'D'.
        APPEND ls_action TO lr_action.

        ls_action-low = 'F'.
        APPEND ls_action TO lr_action.
    ENDCASE.

  ELSE.

    RAISE internal_error.

  ENDIF.


********************************************
* Defaultwerte Customizing ermitteln
********************************************

****************************
* Allg. Defaultwerte holen *
****************************

* Aus Basic-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen
  SELECT * FROM zvc_basic_cfg INTO CORRESPONDING FIELDS OF TABLE lt_basic_cfg
    WHERE type = lv_type AND
          ( model = lv_model OR
            model = '*' ) AND
*          action IN lr_action AND
          datuv <= globals-date AND
          datub > globals-date
    ORDER BY atnam.

* Copy lt_basic_cfg into lt_default_vals which is processed and updated further down
*  lt_default_vals = lt_basic_cfg.
*  DELETE lt_default_vals WHERE NOT action IN lr_action.
* New 740 syntax with FILTER
  lt_default_vals = FILTER #( lt_basic_cfg USING KEY action WHERE action = 'D' ).

* Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
* Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
  LOOP AT lt_default_vals ASSIGNING <fs_default_vals>.

    AT NEW atnam.

* Hilfstabelle lt_default_vals_2 aufbauen; diese enthält die Einträge für das aktuelle ATNAM
      LOOP AT lt_default_vals ASSIGNING <fs_default_vals_2>
                              WHERE atnam = <fs_default_vals>-atnam.
        APPEND <fs_default_vals_2> TO lt_default_vals_2.
      ENDLOOP.

* Enthält die Tabelle mindestens ein Modell und mindestens einen Sterneintrag?:
*     Bemerkung: Mit der Einführung von mehreren Default-Werten pro Merkmal reicht die Prüfung auf die Länger der Tabelle nicht mehr aus:
*         * Wie lang ist diese Hilfstabelle? 1 oder 2 Einträge?
*         DESCRIBE TABLE lt_default_vals_2 LINES lv_lines.

      READ TABLE lt_default_vals_2 WITH KEY model = '*' TRANSPORTING NO FIELDS.
      IF sy-subrc = 0.
        READ TABLE lt_default_vals_2 WITH KEY model = lv_model TRANSPORTING NO FIELDS.

* 2 Einträge => Für dieses ATNAM gibt es Eintrag mit spezifischem Modell und mit * => Den allgemeinen Eintrag (Modell = *) aus der
* ursprünglichen int. Tabelle löschen
        IF sy-subrc = 0.

          LOOP AT lt_default_vals ASSIGNING <fs_default_vals_2>
                                  WHERE model = '*' AND
                                        atnam = <fs_default_vals>-atnam.

            APPEND <fs_default_vals_2> TO lt_default_vals_del.

          ENDLOOP.

        ENDIF.
      ENDIF.

* Hilfstabelle leeren.
      REFRESH lt_default_vals_2.

    ENDAT.

  ENDLOOP.

*Tabelleneinträge löschen, die zuvor ausgeschlossen wurden.
  LOOP AT lt_default_vals_del ASSIGNING <fs_default_vals>.
    DELETE TABLE lt_default_vals FROM <fs_default_vals>.
  ENDLOOP.

*********************************************
* Spezifische Defaultwerte der Region holen *
*********************************************

* Aus Region-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen
  SELECT * FROM zvc_region_cfg INTO CORRESPONDING FIELDS OF TABLE lt_region_cfg
    WHERE type = lv_type AND
          ( model = lv_model OR
            model = '*' ) AND
          region = lv_region AND
*          action IN lr_action AND
          datuv <= globals-date AND
          datub > globals-date
    ORDER BY atnam.

* Copy lt_region_cfg into lt_default_vals which is processed and updated further down
*  lt_region_cfg_def = lt_region_cfg.
*  DELETE lt_region_cfg_def WHERE NOT action IN lr_action.
* New 740 syntax with FILTER
  lt_region_cfg_def = FILTER #( lt_region_cfg USING KEY action WHERE action = 'D' ).

* Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
* Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
  LOOP AT lt_region_cfg_def ASSIGNING <fs_region_cfg>.

    AT NEW atnam.

* Hilfstabelle (xxx_2) aufbauen; diese enthält die Einträge für das aktuelle ATNAM
      LOOP AT lt_region_cfg_def ASSIGNING <fs_region_cfg_2>
                                WHERE atnam = <fs_region_cfg>-atnam.
        APPEND <fs_region_cfg_2> TO lt_region_cfg_2.
      ENDLOOP.

* Enthält die Tabelle mindestens ein Modell und mindestens einen Sterneintrag?:
*     Bemerkung: Mit der Einführung von mehreren Default-Werten pro Merkmal reicht die Prüfung auf die Länger der Tabelle nicht mehr aus:
*         * Wie lang ist diese Hilfstabelle? 1 oder 2 Einträge?
*         DESCRIBE TABLE lt_region_cfg_2 LINES lv_lines.

      READ TABLE lt_region_cfg_2 WITH KEY model = '*' TRANSPORTING NO FIELDS.
      IF sy-subrc = 0.
        READ TABLE lt_region_cfg_2 WITH KEY model = lv_model TRANSPORTING NO FIELDS.

* 2 Einträge => Für dieses ATNAM gibt es Eintrag mit spezifischem Modell und mit * => Den allgemeinen Eintrag (Modell = *) aus der
* ursprünglichen int. Tabelle löschen
        IF sy-subrc = 0.

          LOOP AT lt_region_cfg_def ASSIGNING <fs_region_cfg_2>
                                    WHERE model = '*' AND
                                          atnam = <fs_region_cfg>-atnam.

            APPEND <fs_region_cfg_2> TO lt_region_cfg_del.

          ENDLOOP.

        ENDIF.
      ENDIF.
    ENDAT.

* Hilfstabelle leeren.
    REFRESH lt_region_cfg_2.

  ENDLOOP.

*Tabelleneinträge löschen, die zuvor ausgeschlossen wurden.
  LOOP AT lt_region_cfg_del ASSIGNING <fs_region_cfg>.
    DELETE TABLE lt_region_cfg_def FROM <fs_region_cfg>.
  ENDLOOP.

* Update der Tabelle lt_default_vals mit den Region-spezifischen Defaultwerten
  LOOP AT lt_region_cfg_def ASSIGNING <fs_region_cfg>.

    READ TABLE lt_default_vals ASSIGNING <fs_default_vals> WITH KEY atnam = <fs_region_cfg>-atnam.
    IF sy-subrc = 0.
      <fs_default_vals>-atwrt     = <fs_region_cfg>-atwrt.
    ELSE.
      APPEND INITIAL LINE TO lt_default_vals ASSIGNING <fs_default_vals>.
      <fs_default_vals>-type      = <fs_region_cfg>-type.
      <fs_default_vals>-model     = <fs_region_cfg>-model.
      <fs_default_vals>-atnam     = <fs_region_cfg>-atnam.
      <fs_default_vals>-atwrt     = <fs_region_cfg>-atwrt.
      <fs_default_vals>-action    = <fs_region_cfg>-action.
    ENDIF.

  ENDLOOP.

* P. Muthsam, SAP, 12.03.2020: Defaultwerte, die aus einer der vorangegangenen, allgemeineren Tabellen geholt wurden,
* müssen gelöscht werden, falls die Region-Tabelle zwar Include-Einträge, aber keinen Default-Eintrag für das Merkmal hat
  LOOP AT lt_default_vals ASSIGNING <fs_default_vals>.

* Gibt es Include-Einträge?
    READ TABLE lt_region_cfg TRANSPORTING NO FIELDS
                             WITH KEY atnam = <fs_default_vals>-atnam
                                      action = 'I'.

    IF sy-subrc = 0.

* Gab es einen Default-Eintrag?
      READ TABLE lt_region_cfg TRANSPORTING NO FIELDS
                               WITH KEY atnam = <fs_default_vals>-atnam
*                                          action in lr_action.       " Syntax 'in <range>' not allowed for KEY in READ TABLE
                                          action = 'D'.

* Nein -> dann müssen wir den Eintrag aus der lt_defaults_val löschen
      IF sy-subrc <> 0.

        DELETE lt_default_vals WHERE atnam = <fs_default_vals>-atnam.

      ENDIF.  " Kein Default-Eintrag vorhanden

    ENDIF.    " Include-Einträge vorhanden

  ENDLOOP.    " AT lt_default_vals



*********************************************
* Spezifische Defaultwerte des Landes holen *
*********************************************

* Aus Country-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen
  SELECT * FROM zvc_country_cfg INTO CORRESPONDING FIELDS OF TABLE lt_country_cfg
    WHERE type = lv_type AND
          ( model = lv_model OR
            model = '*' ) AND
          country = lv_country AND
*          action IN lr_action AND
          datuv <= globals-date AND
          datub > globals-date
    ORDER BY atnam.

* Copy lt_country_cfg into lt_default_vals which is processed and updated further down
*  lt_country_cfg_def = lt_country_cfg.
*  DELETE lt_country_cfg_def WHERE NOT action IN lr_action.
* New 740 syntax with FILTER
  lt_country_cfg_def = FILTER #( lt_country_cfg USING KEY action WHERE action = 'D' ).

* Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
* Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
  LOOP AT lt_country_cfg_def ASSIGNING <fs_country_cfg>.

    AT NEW atnam.

* Hilfstabelle (xxx_2) aufbauen; diese enthält die Einträge für das aktuelle ATNAM
      LOOP AT lt_country_cfg_def ASSIGNING <fs_country_cfg_2>
                                 WHERE atnam = <fs_country_cfg>-atnam.
        APPEND <fs_country_cfg_2> TO lt_country_cfg_2.
      ENDLOOP.

* Enthält die Tabelle mindestens ein Modell und mindestens einen Sterneintrag?:
*     Bemerkung: Mit der Einführung von mehreren Default-Werten pro Merkmal reicht die Prüfung auf die Länger der Tabelle nicht mehr aus:
*         * Wie lang ist diese Hilfstabelle? 1 oder 2 Einträge?
*         DESCRIBE TABLE lt_country_cfg_2 LINES lv_lines.

      READ TABLE lt_country_cfg_2 WITH KEY model = '*' TRANSPORTING NO FIELDS.
      IF sy-subrc = 0.
        READ TABLE lt_country_cfg_2 WITH KEY model = lv_model TRANSPORTING NO FIELDS.

* 2 Einträge => Für dieses ATNAM gibt es Eintrag mit spezifischem Modell und mit * => Den allgemeinen Eintrag (Modell = *) aus der
* ursprünglichen int. Tabelle löschen
        IF sy-subrc = 0.

          LOOP AT lt_country_cfg_def ASSIGNING <fs_country_cfg_2>
                                     WHERE model = '*' AND
                                           atnam = <fs_country_cfg>-atnam.

            APPEND <fs_country_cfg_2> TO lt_country_cfg_del.

          ENDLOOP.

        ENDIF.
      ENDIF.
    ENDAT.

* Hilfstabelle leeren.
    REFRESH lt_country_cfg_2.

  ENDLOOP.

*Tabelleneinträge löschen, die zuvor ausgeschlossen wurden.
  LOOP AT lt_country_cfg_del ASSIGNING <fs_country_cfg>.
    DELETE TABLE lt_country_cfg_def FROM <fs_country_cfg>.
  ENDLOOP.

* Update der Tabelle lt_default_vals mit den country-spezifischen Defaultwerten
  LOOP AT lt_country_cfg_def ASSIGNING <fs_country_cfg>.

    READ TABLE lt_default_vals ASSIGNING <fs_default_vals> WITH KEY atnam = <fs_country_cfg>-atnam.
    IF sy-subrc = 0.
      <fs_default_vals>-atwrt     = <fs_country_cfg>-atwrt.
    ELSE.
      APPEND INITIAL LINE TO lt_default_vals ASSIGNING <fs_default_vals>.
      <fs_default_vals>-type      = <fs_country_cfg>-type.
      <fs_default_vals>-model     = <fs_country_cfg>-model.
      <fs_default_vals>-atnam     = <fs_country_cfg>-atnam.
      <fs_default_vals>-atwrt     = <fs_country_cfg>-atwrt.
      <fs_default_vals>-action    = <fs_country_cfg>-action.
    ENDIF.

  ENDLOOP.

* P. Muthsam, SAP, 12.03.2020: Defaultwerte, die aus einer der vorangegangenen, allgemeineren Tabellen geholt wurden,
* müssen gelöscht werden, falls die Country-Tabelle zwar Include-Einträge, aber keinen Default-Eintrag für das Merkmal hat
  LOOP AT lt_default_vals ASSIGNING <fs_default_vals>.

* Gibt es Include-Einträge?
    READ TABLE lt_country_cfg TRANSPORTING NO FIELDS
                              WITH KEY atnam = <fs_default_vals>-atnam
                                       action = 'I'.

    IF sy-subrc = 0.

* Gab es einen Default-Eintrag?
      READ TABLE lt_country_cfg TRANSPORTING NO FIELDS
                                WITH KEY atnam = <fs_default_vals>-atnam
*                                         action in lr_action.       " Syntax 'in <range>' not allowed for KEY in READ TABLE
                                           action = 'D'.

* Nein -> dann müssen wir den Eintrag aus der lt_defaults_val löschen
      IF sy-subrc <> 0.

        DELETE lt_default_vals WHERE atnam = <fs_default_vals>-atnam.

      ENDIF.  " Kein Default-Eintrag vorhanden

    ENDIF.    " Include-Einträge vorhanden

  ENDLOOP.    " AT lt_default_vals



*******************************************************
* Spezifische Defaultwerte der Sonderausführung holen *
*******************************************************

* Aus Special-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen
  SELECT * FROM zvc_special_cfg INTO CORRESPONDING FIELDS OF TABLE lt_special_cfg
    WHERE type = lv_type AND
          ( model = lv_model OR
            model = '*' ) AND
          sversion = lv_sversion AND
*          action IN lr_action AND
          datuv <= globals-date AND
          datub > globals-date
    ORDER BY atnam.

* Copy lt_special_cfg into lt_default_vals which is processed and updated further down
*  lt_special_cfg_def = lt_special_cfg.
*  DELETE lt_special_cfg_def WHERE NOT action IN lr_action.
* New 740 syntax with FILTER
  lt_special_cfg_def = FILTER #( lt_special_cfg USING KEY action WHERE action = 'D' ).

* Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
* Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
  LOOP AT lt_special_cfg_def ASSIGNING <fs_special_cfg>.

    AT NEW atnam.

* Hilfstabelle (xxx_2) aufbauen; diese enthält die Einträge für das aktuelle ATNAM
      LOOP AT lt_special_cfg_def ASSIGNING <fs_special_cfg_2>
                                 WHERE atnam = <fs_special_cfg>-atnam.
        APPEND <fs_special_cfg_2> TO lt_special_cfg_2.
      ENDLOOP.

* Enthält die Tabelle mindestens ein Modell und mindestens einen Sterneintrag?:
*     Bemerkung: Mit der Einführung von mehreren Default-Werten pro Merkmal reicht die Prüfung auf die Länger der Tabelle nicht mehr aus:
*         * Wie lang ist diese Hilfstabelle? 1 oder 2 Einträge?
*         DESCRIBE TABLE lt_special_cfg_2 LINES lv_lines.

      READ TABLE lt_special_cfg_2 WITH KEY model = '*' TRANSPORTING NO FIELDS.
      IF sy-subrc = 0.
        READ TABLE lt_special_cfg_2 WITH KEY model = lv_model TRANSPORTING NO FIELDS.

* 2 Einträge => Für dieses ATNAM gibt es Eintrag mit spezifischem Modell und mit * => Den allgemeinen Eintrag (Modell = *) aus der
* ursprünglichen int. Tabelle löschen
        IF sy-subrc = 0.

          LOOP AT lt_special_cfg_def ASSIGNING <fs_special_cfg_2>
                                     WHERE model = '*' AND
                                           atnam = <fs_special_cfg>-atnam.

            APPEND <fs_special_cfg_2> TO lt_special_cfg_del.

          ENDLOOP.

        ENDIF.
      ENDIF.
    ENDAT.

* Hilfstabelle leeren.
    REFRESH lt_special_cfg_2.

  ENDLOOP.

*Tabelleneinträge löschen, die zuvor ausgeschlossen wurden.
  LOOP AT lt_special_cfg_del ASSIGNING <fs_special_cfg>.
    DELETE TABLE lt_special_cfg_def FROM <fs_special_cfg>.
  ENDLOOP.

* Update der Tabelle lt_default_vals mit den Sonderausführung-spezifischen Defaultwerten
  LOOP AT lt_special_cfg_def ASSIGNING <fs_special_cfg>.

    READ TABLE lt_default_vals ASSIGNING <fs_default_vals> WITH KEY atnam = <fs_special_cfg>-atnam.
    IF sy-subrc = 0.
      <fs_default_vals>-atwrt     = <fs_special_cfg>-atwrt.
    ELSE.
      APPEND INITIAL LINE TO lt_default_vals ASSIGNING <fs_default_vals>.
      <fs_default_vals>-type      = <fs_special_cfg>-type.
      <fs_default_vals>-model     = <fs_special_cfg>-model.
      <fs_default_vals>-atnam     = <fs_special_cfg>-atnam.
      <fs_default_vals>-atwrt     = <fs_special_cfg>-atwrt.
      <fs_default_vals>-action    = <fs_special_cfg>-action.
    ENDIF.

  ENDLOOP.

* P. Muthsam, SAP, 12.03.2020: Defaultwerte, die aus einer der vorangegangenen, allgemeineren Tabellen geholt wurden,
* müssen gelöscht werden, falls die Country-Tabelle zwar Include-Einträge, aber keinen Default-Eintrag für das Merkmal hat
  LOOP AT lt_default_vals ASSIGNING <fs_default_vals>.

* Gibt es Include-Einträge?
    READ TABLE lt_special_cfg TRANSPORTING NO FIELDS
                              WITH KEY atnam = <fs_default_vals>-atnam
                                       action = 'I'.

    IF sy-subrc = 0.

* Gab es einen Default-Eintrag?
      READ TABLE lt_special_cfg TRANSPORTING NO FIELDS
                                WITH KEY atnam = <fs_default_vals>-atnam
*                                         action in lr_action.       " Syntax 'in <range>' not allowed for KEY in READ TABLE
                                           action = 'D'.

* Nein -> dann müssen wir den Eintrag aus der lt_defaults_val löschen
      IF sy-subrc <> 0.

        DELETE lt_default_vals WHERE atnam = <fs_default_vals>-atnam.

      ENDIF.  " Kein Default-Eintrag vorhanden

    ENDIF.    " Include-Einträge vorhanden

  ENDLOOP.    " AT lt_default_vals



********************************************************************************************************************
* Default-Werte löschen, die nicht mehr gesetzt werden (z.B. aufgrund des Konfigurations-Updates nach User-Eingabe)
********************************************************************************************************************

* P. Muthsam, SAP, 13.06.2018:
* Welche Default-Werte können überhaupt aus dieser VF gesetzt werden? Nur diese dürfen wiederum von der VF gelöscht werden.
* Hier wird nicht nach dem Wert von Modell selektiert, weil das Modell geändert werden kann. Somit müssen wir alle möglichen
* Merkmale (ATNAM) über alle Modelle hinweg berücksichtigen.
  SELECT atnam FROM zvc_basic_cfg INTO TABLE @DATA(lt_default_cstics)
    WHERE type = @lv_type AND
          action = 'D' AND
          datuv <= @globals-date AND
          datub > @globals-date.

*  lt_default_cstics = VALUE #( FOR GROUPS cstic_name OF <line> IN lt_basic_cfg GROUP BY <line>-atnam ASCENDING WITHOUT MEMBERS ( cstic_name ) ).


  SELECT atnam FROM zvc_region_cfg INTO TABLE @DATA(lt_default_cstics_region)
    WHERE type   = @lv_type AND
          region = @lv_region AND
          action = 'D' AND
          datuv <= @globals-date AND
          datub > @globals-date.

  APPEND LINES OF lt_default_cstics_region TO lt_default_cstics.

  SELECT atnam FROM zvc_country_cfg INTO TABLE @DATA(lt_default_cstics_country)
    WHERE type   = @lv_type AND
          country = @lv_country AND
          action IN @lr_action AND
          datuv <= @globals-date AND
          datub > @globals-date.

  APPEND LINES OF lt_default_cstics_country TO lt_default_cstics.

  SELECT atnam FROM zvc_special_cfg INTO TABLE @DATA(lt_default_cstics_special)
    WHERE type = @lv_type AND
          sversion = @lv_sversion AND
          action = 'D' AND
          datuv <= @globals-date AND
          datub > @globals-date.

  APPEND LINES OF lt_default_cstics_special TO lt_default_cstics.

  SORT lt_default_cstics BY atnam.
  DELETE ADJACENT DUPLICATES FROM lt_default_cstics COMPARING atnam.


* Hole alle Merkmale mit ihren Werten

  CALL FUNCTION 'CUPR_GET_VALLIST_INST'
    EXPORTING
      instance       = globals-self
*     RES_AREA       = ' '
    IMPORTING
      vallist        = lt_vallist
*     RESTRICTION    =
    EXCEPTIONS
      internal_error = 1
      wrong_context  = 2
      OTHERS         = 3.


* Now loop over all cstics and delete default values from cstics that are not in table lt_default_vals with the updated default values

  IF sy-subrc = 0.

    LOOP AT lt_vallist INTO ls_vallist.

* We have only single-value cstics in the 4 tables -> line 1 from table values
      READ TABLE ls_vallist-values INTO ls_value INDEX 1.
      IF sy-subrc <> 0.
        CONTINUE.
      ENDIF.

* Is this cstic a default value 'candidate' from our 4 tables?
      READ TABLE lt_default_cstics WITH KEY atnam = ls_vallist-atnam TRANSPORTING NO FIELDS.

* If cstic is not in our 4 CFG tables for default values, then do not delete its value
      IF sy-subrc <> 0.
        CONTINUE.
      ENDIF.

* Delete existing default value if not included in the table with the updated default values (lt_default_vals)
* A value that is changed does not have to be deleted - setting a different default value (further down with
* CUPR_SET_DEFAULT) is sufficient.
      READ TABLE lt_default_vals ASSIGNING <fs_default_vals>
                                 WITH KEY atnam = ls_vallist-atnam.
      lv_sysubrc = sy-subrc.

* Zählen der Tabelle aufgrund einer Inkonsistenz, die durch einen vorher gesetzten Default-Wert ausgelöst wird.
* Die Werteeinschränkeung des Constraints würde aufgrund des Defaultwertes (alter) erfolgen eine Einschränkung seitens des "Default"-Wertes
* erhalten.
      IF sy-subrc = 0.
        lv_lines = REDUCE i( INIT x = 0
                                   FOR <l> IN lt_default_vals WHERE ( atnam = <fs_default_vals>-atnam )
                                   NEXT x = x + 1 ).
      ENDIF.

      IF lv_sysubrc <> 0 OR                         " Merkmal it nicht in der Tabelle mit den "neuen" Defaultwerten
         lv_lines > 1 OR                            " Mehr als ein Default-Wert pro Merkmal
         ls_value-atwrt <> <fs_default_vals>-atwrt. " Wechsel vom Land (Default-Wert kam aus der Basis-Tabelle / Default beim zweiten Durchlauf aus der Country-Tabelle)
        " --> Somit jegliche Konfigurationsänderungen, die andere Default-Werte aus den 4 Konfigurationstabellen auslesen.
        " 12.03.2020

        CLEAR ls_val.
        ls_val-atfor = 'CHAR'.
        ls_val-atwrt = ls_value-atwrt.

        CALL FUNCTION 'CUPR_DEL_DEFAULT'
          EXPORTING
            instance               = globals-self
            characteristic         = ls_vallist-atnam
            val                    = ls_val
          EXCEPTIONS
            unknown_instance       = 1
            unknown_characteristic = 2
            not_deleted            = 3
            internal_error         = 4
            wrong_context          = 5
            OTHERS                 = 6.
        IF sy-subrc <> 0.
          CONTINUE.
        ENDIF.

* Defaultwert ändert sich nicht -> muss auch nicht neu gesetzt werden -> Eintrag aus lt_default_vals löschen
      ELSEIF <fs_default_vals> IS ASSIGNED AND
             ls_value-atwrt = <fs_default_vals>-atwrt.

        DELETE TABLE lt_default_vals FROM <fs_default_vals>.

      ENDIF.

    ENDLOOP.

  ENDIF.


********************************************
* Geänderte und neue Defaultwerte setzen
********************************************
  LOOP AT lt_default_vals ASSIGNING <fs_default_vals>.

    CLEAR ls_val.
    ls_val-atfor = 'CHAR'.
    ls_val-atwrt = <fs_default_vals>-atwrt.

    CALL FUNCTION 'CONVERSION_EXIT_ATINN_INPUT'
      EXPORTING
        input  = <fs_default_vals>-atnam
      IMPORTING
        output = lv_atinn.


    CALL FUNCTION 'CUDB_GET_CURRENT_DOM'
      EXPORTING
        atinn          = lv_atinn
        instance       = globals-self
        rflag          = '1'
* IMPORTING
*       DOMAIN_INDEX   =
      TABLES
        domain         = lt_domain
      EXCEPTIONS
        internal_error = 1
        no_value_found = 2
        unconstrained  = 3
        OTHERS         = 4.
    IF sy-subrc <> 0.
      CONTINUE.
    ENDIF.

    READ TABLE lt_domain WITH KEY atwrt = <fs_default_vals>-atwrt TRANSPORTING NO FIELDS.
    IF sy-subrc <> 0.
      CONTINUE.
    ENDIF.

    CASE <fs_default_vals>-action.
      WHEN 'D'.
        CALL FUNCTION 'CUPR_SET_DEFAULT'
          EXPORTING
            instance               = globals-self
            characteristic         = <fs_default_vals>-atnam
            val                    = ls_val
          EXCEPTIONS
            unknown_instance       = 1
            unknown_characteristic = 2
            internal_error         = 3
            wrong_context          = 4
            OTHERS                 = 5.

*   Falls nicht vorhandes Merkmal in der Tabelle gepflegt, nicht aussteigen, sondern Fehler ignorieren
        IF sy-subrc <> 0.
          CONTINUE.
        ENDIF.

      WHEN 'F'.
        CALL FUNCTION 'CUPR_SET_VAL'
          EXPORTING
            instance               = globals-self
            characteristic         = <fs_default_vals>-atnam
            val                    = ls_val
          EXCEPTIONS
            unknown_instance       = 1
            unknown_characteristic = 2
            internal_error         = 3
            OTHERS                 = 4.

*   Falls nicht vorhandes Merkmal in der Tabelle gepflegt, nicht aussteigen, sondern Fehler ignorieren
        IF sy-subrc <> 0.
          CONTINUE.
        ENDIF.

    ENDCASE.

  ENDLOOP.


ENDFUNCTION.