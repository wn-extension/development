FORM FRM_KOND_BASIS_957.
*{   INSERT         E3DK975978                                        1

* [CR309678] Net prices in variant configuration
   DATA: lt_xkomv TYPE TABLE OF komv_index.

   lt_xkomv[] = xkomv[].

   LOOP AT lt_xkomv INTO DATA(ls_xkomv)
     WHERE kposn = komp-kposn
       AND kschl = 'ZVAN'
       AND kinak = ''. "active

     xkwert      = xkwert      - ls_xkomv-kbetr.

   ENDLOOP.
* [CR309678] Net prices in variant configuration

*}   INSERT
ENDFORM.
