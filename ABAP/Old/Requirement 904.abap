*---------------------------------------------------------------------*
*       FORM KOBED_904                                                *
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
FORM kobed_904.

  DATA: lt_comw    LIKE comw OCCURS 0 WITH HEADER LINE,
        l_cuobf    TYPE cuobm,
        l_merkmal  TYPE atnam,
        l_wert(3),
        l_paket(3),
        l_subrc    LIKE sy-subrc,
        l_varcond  LIKE komp-varcond.

  IF sy-uname = 'SRB0_SANYTR'.
*    break-point.
  ENDIF.

  l_subrc = sy-subrc.

  READ TABLE xkomv WITH KEY varcond(5) = 'A_PAK'.
*VC NEU - 09.04.2020
  IF sy-subrc <> 0.
    READ TABLE xkomv WITH KEY varcond(10) = 'A_CGL_PACK'.
  ENDIF.
  IF sy-subrc <> 0.
    READ TABLE xkomv WITH KEY varcond(13) = 'A_CGL_EDITION'.
  ENDIF.

  IF sy-subrc EQ 0.

    SELECT SINGLE cuobf FROM  mara INTO l_cuobf
           WHERE  matnr  = komp-matnr.

*VC NEU - 09.04.2020
    IF l_cuobf IS INITIAL.
      READ TABLE xkomv WITH KEY varcond(10) = 'A_CGL_PACK'.
      IF sy-subrc <> 0.
        READ TABLE xkomv WITH KEY varcond(13) = 'A_CGL_EDITION'.
      ENDIF.
      IF sy-subrc = 0.
        SELECT SINGLE cuobj FROM vbap INTO l_cuobf
          WHERE matnr = komp-matnr
          AND vbeln = komp-aubel.
      ENDIF.
    ENDIF.
    IF sy-subrc EQ 0.

* Bewertung TYP aus Materialstamm -------------------------------*
*-----
      CALL FUNCTION 'CUD0_GET_VAL_FROM_INSTANCE'
        EXPORTING
          instance   = l_cuobf
*    IMPORTING
*         TYPE_OF    =
*         OWNER      =
        TABLES
          attributes = lt_comw.
*    EXCEPTIONS
*      INSTANCE_NOT_FOUND       = 1
*      OTHERS                   = 2
      .
      READ TABLE lt_comw WITH KEY atinn = '0000000004'. "PR_TYP = '0000000004'
      IF sy-subrc = 0.
* Ist eine 'Paket'-Kondition vorhanden ?
        LOOP AT xkomv WHERE varcond(5) = 'A_PAK'.
          l_paket = xkomv-varcond+5(3).

*
          SELECT zzmerkmal zzwert FROM  zzpaket
                 INTO (l_merkmal, l_wert)
                 WHERE  zztyp      = lt_comw-atwrt
                 AND    zzpaket    = l_paket.

            l_varcond      = l_merkmal.
            l_varcond+20   = '_'.
            l_varcond+21   = l_wert.
            CONDENSE l_varcond NO-GAPS.

            IF komp-varcond = l_varcond.
              l_subrc = 4.
              EXIT.
            ENDIF.
          ENDSELECT.
        ENDLOOP.

      ELSEIF sy-subrc <> 0.
*   VC NEU A_CGL_PACK
        READ TABLE lt_comw WITH KEY atinn = '0000008048'. "CGL_TYPE = '0000008048'
        IF sy-subrc = 0.
          READ TABLE xkomv WITH KEY varcond(10) = 'A_CGL_PACK'.
          IF sy-subrc = 0.
            LOOP AT xkomv WHERE varcond(10) = 'A_CGL_PACK'.
              l_paket = xkomv-varcond+11(3).

              SELECT atnam atwrt FROM zvc_pack
                INTO (l_merkmal, l_wert)
                WHERE type = lt_comw-atwrt
                AND pack_id = l_paket.

                l_varcond = l_merkmal.
                l_varcond+20 = '_'.
                l_varcond+21 = l_wert.
                CONDENSE l_varcond NO-GAPS.

                IF komp-varcond = l_varcond.
                  l_subrc = 4.
                  EXIT.
                ENDIF.
              ENDSELECT.
            ENDLOOP.

          ELSEIF sy-subrc <> 0.
            READ TABLE xkomv WITH KEY varcond(13) = 'A_CGL_EDITION'.
            IF sy-subrc = 0.
              LOOP AT xkomv WHERE varcond(13) = 'A_CGL_EDITION'.
                l_paket = xkomv-varcond+14(3).

                SELECT atnam atwrt FROM zvc_edition
                  INTO (l_merkmal, l_wert)
                  WHERE type = lt_comw-atwrt
                  AND edition_id = l_paket.

                  l_varcond = l_merkmal.
                  l_varcond+21 = '_'.
                  l_varcond+22 = l_wert.
                  CONDENSE l_varcond NO-GAPS.

                  IF komp-varcond = l_varcond.
                    l_subrc = 4.
                    EXIT.
                  ENDIF.
                ENDSELECT.
              ENDLOOP.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.

    ENDIF.

    IF NOT l_subrc IS INITIAL.
      sy-subrc = 4.
    ENDIF.

  ELSE.
    sy-subrc = l_subrc.
  ENDIF.

  CLEAR l_subrc.

ENDFORM.





*---------------------------------------------------------------------*
*       FORM KOBEV_904                                                *
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
FORM kobev_904.
  IF sy-uname = 'SRB0_SANYTR'.
    BREAK-POINT.
  ENDIF.
ENDFORM.