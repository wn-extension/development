FUNCTION zvc_set_environment.
*"----------------------------------------------------------------------
*"*"Lokale Schnittstelle:
*"  IMPORTING
*"     REFERENCE(GLOBALS) LIKE  CUOV_00 STRUCTURE  CUOV_00
*"  TABLES
*"      QUERY STRUCTURE  CUOV_01
*"      MATCH STRUCTURE  CUOV_01
*"  EXCEPTIONS
*"      FAIL
*"      INTERNAL_ERROR
*"----------------------------------------------------------------------


* P. Muthsam, SAP & J. Müller 06.05.2019:
* Setzt das Environment-Merkmal für die Konfiguration --> ECC = VC --> Überschreibt den Default-Wert "IPC"
* CRM = IPC - wird hier NICHT gesetzt, sondern als Defulat-Wert über die aufrufende Prozedur

  CONSTANTS: lc_atnam_environment  TYPE atnam VALUE 'CGL_ENVIRONMENT'.

* Funktion CUOV_SET_FUNCTION_ARGUMENT auskommentiert, da die pfunction den Wert nicht setzt.
  CALL FUNCTION 'CUOV_SET_FUNCTION_ARGUMENT'
    EXPORTING
      argument                = lc_atnam_environment
      vtype                   = 'CHAR'
      sym_val                 = 'VC'
*     NUM_VAL                 =
    TABLES
      match                   = match
    EXCEPTIONS
      existing_value_replaced = 1
      OTHERS                  = 2.

* Errorhandling für die Funktion CUOV_SET_FUNCTION_ARGUMENT
  IF sy-subrc = 2.
    MESSAGE e208(00) WITH 'Error in ZVC_SET_ENVIRONMENT'.
  ENDIF.



ENDFUNCTION.