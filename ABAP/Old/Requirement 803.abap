*{   INSERT         E3DK920430                                        1
*&---------------------------------------------------------------------*
*& Vorname Nachname: Marc-Kevin Thöne
*& erstellt am     : 11.03.2016
*&---------------------------------------------------------------------*
*& CR Nummer       : IR80468
*& Business Analyst: Hr. Thöne
*& Beschreibung    : Konditionswertformel zur Korrektur von
*&                   Rundungsdifferenzen (Hinweis 80183)
*&
*&---------------------------------------------------------------------*
*& Änderungen (wer / wann / was):
*& (Kürzel)XXX, tt.mm.yyyy, kurze Beschreibung
*&---------------------------------------------------------------------*
*
*}   INSERT
FORM FRM_KONDI_WERT_803.
*{   INSERT         E3DK920430                                        1

  ykwert = xkwert.
  ywaers = xkomv-waers.
  ykbetr = xkomv-kbetr.

*}   INSERT
ENDFORM.
