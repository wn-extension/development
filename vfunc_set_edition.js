/*
Variant function ZVC_SET_EDITION_2 for Wacker-Neuson
Adapted of ABAP function ZVC_SET_EDITION_2 implemented by Peter Muthsam / SAP
Reimplemented in nodeJS by Matthias Plietz / 2022-02-07
2022-02-09: new DB access, cstic vals w/o parseInt, table access with schema, table access with date
*/
'use strict';

//-------------------------------------------------
//Function ZVC_SET_EDITION_2
//implementation of variant function ZVC_SET_EDITION_2 like similar ABAP function module
//remark: comments in German like corresponding code sections of ABAP
//
module.exports.ZVC_SET_EDITION_2 = function (fnArgs) {
	const cstic_template = { id: "id", type: "String", values: [] };
	const { PerformanceObserver, performance } = require('perf_hooks');


	var c = {};
	var value = "";
	var result = true;
	var fnRes = [];

	//  detect input cstics
	var v_CGL_TYPE = fnArgs.find(({ id }) => id === 'CGL_TYPE').values[0];
	var v_CGL_MODEL = fnArgs.find(({ id }) => id === 'CGL_MODEL').values[0];
	var v_CGL_EDITION = fnArgs.find(({ id }) => id === 'CGL_EDITION').values[0];
	var today = new Date().toISOString();

	var lt_edition_vals = [];
	var lt_vals = [];

	//  establish connection
	var repo = new (require('./helper/repo'));
	var connection = repo.getConnection();

	//****************************
	//* Alle Werte für Paket auslesen
	//****************************
	//  Spezifische Defaultwerte für Type holen
	var sql = "select * from " + repo.getSchema() + "ZVC_EDITION where type = ? and ( model = ? or model = '*' ) and edition_id = ? "
		+ " and datuv <= ? and datub > ?"
		+ " order by atnam";
	var params = [v_CGL_TYPE, v_CGL_MODEL, v_CGL_EDITION, today, today];
	lt_vals = connection.exec(sql, params);

	//  test:
	//	sql = "select * from " + c_DbScema + "ZVC_EDITION where type = ?";
	//  params = [ v_CGL_TYPE ];
	//	lt_vals = connection.exec(sql, params);

	//********************************************
	//* Relevanten Eintrag pro Merkmalswert ermitteln
	//********************************************	
	//  Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
	//  Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	var lt_del = getDuplicates(lt_vals);
	//	Tabelleneinträge löschen, die zuvor ausgeschlossen wurden.
	//  also den * Eintrag des vorkommenden Merkmals
	lt_edition_vals = lt_vals.filter(f => !(lt_del.includes(f.ATNAM) && f.MODEL == '*'))

	connection.close();
	//	********************************************
	//	* Paket-werte setzen
	//	********************************************

	lt_edition_vals.forEach(function (row) {
		var cstic = { id: "id", type: "String", values: [] };
		cstic.id = row.ATNAM;
		value = "000" + row.ATWRT;
		value = value.substring(value.length - 3);  //numerischen Wert wieder mit leading zeros 
		cstic.values.push(value);
		fnRes.push(cstic);
		require('./helper/helper.js').logging(cstic);
	});

	var result = { status: 200, data: { type: "vfun", vfunOutput: { result: result, fnArgs: fnRes } } };
	return result;
}



//-------------------------------------------------
//Function getDuplicates
//Selection of cstic names assigned to both a concrete model and its widecard *
//List is to delete the * table entries in next processing step
//remarks: in difference to ABAP this function is extracted and refactored for several calls, 
//         and the comments are in German like corresponding code sections of ABAP
//
function getDuplicates(lt_vals) {
	// Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
	// Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	var c_prevATNAM = '';
	var lt_del = [];
	lt_vals.forEach(function (row) {
		if (row.ATNAM != c_prevATNAM) {
			//          Gruppenwechsel:
			c_prevATNAM = row.ATNAM;
			// 			Hilfstabelle lt_vals_2 aufbauen; diese enthält die Einträge für das aktuelle ATNAM
			var lt_vals_2 = [];
			lt_vals.filter(f => f.ATNAM == row.ATNAM).forEach(function (row) {
				lt_vals_2.push(row);
			});
			//			Wie lang ist diese Hilfstabelle? 1 oder 2 Einträge?			
			var lv_lines = lt_vals_2.length;
			//			Hilfstabelle nach Zählen der Einträge wieder leeren.
			lt_vals_2 = [];

			//			2 Einträge => Für dieses ATNAM gibt es Eintrag mit spezifischem Modell und mit * => Den allgemeinen Eintrag
			//			(Modell = *) aus der ursprünglichen int. Tabelle löschen
			if (lv_lines == 2) {
				lt_del.push(row.ATNAM)
			}
		}
	});
	return lt_del;
}

