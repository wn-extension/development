/*
CPS Exit REST service for Wacker-Neuson
providing NodeJS Adapter for Express used in Cloud Foundry or locally started with NodeJS
calling dispacher functions for variant functions and pricing exits of SAP CPS 
Implemented by Matthias Plietz / 2022-02-02
*/

'use strict';

const express = require('express')
const app = express()
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded


app.get('/', function(req, res) {
    res.status(405).send('{ "message": "call POST /api/v1/customformula!" }')
})

	
app.get('/api/v1/customformula', function(req, res) {
    res.status(405).send('{ "message": "call POST!" }')
})


app.post('/api/v1/customformula', function(req, res) {
	try {
		console.log(JSON.stringify(req.headers));
		var dispatcher = require('./dispatcher.js');
		console.log(JSON.stringify(req.body, null, 0))
		var result = dispatcher.formulaDispatcher(req.body)
		console.log(JSON.stringify(result, null, 0))
		if (result.status != 200)
			res.status(result.status).send(result.data)
		else
			res.send(result.data)
	}
	catch(e) {
		require('./helper/helper.js').error (e.message);
		res.status(404).send('{ "message": "not implemented" }')
	}
})


app.post('/cfguserextension', function(req, res) {
	try {
		console.log(JSON.stringify(req.headers));
		var dispatcher = require('./dispatcher.js');
		console.log(JSON.stringify(req.body, null, 0))
		var result = dispatcher.functionDispatcher(req.body)
		console.log(JSON.stringify(result.data, null, 0))
		if (result.status != 200)
			res.status(result.status).send(result.data)
		else
			res.send(result.data)
	}
	catch(e) {
		require('./helper/helper.js').error (e.message);
		res.status(404).send('{ "message": "not implemented" }')
	}
})


const port = process.env.PORT || 3000;
app.listen(port, function () {
	console.log('Server listens on port ' + port);
})

