const PropertiesReader = require('properties-reader');
var properties = PropertiesReader('config/app.properties');

class props {
    constructor () {


    }

    get(id) {
        return properties.get(id);
    }

    set (id, value) {
        properties.set(id, value);
    }

}
module.exports = props;