const winston = require('winston');
var props = (new (require('./props')));
var logger = winston.createLogger({
    format: winston.format.json(),
    defaultMeta: { service: 'cps-extension' },
});

class logs {

    constructor() {
        if (props.get('Environment') == 'D')
            logger.add(new winston.transports.Console({
                format: winston.format.simple(),
            }))
        else {
            logger.add(new winston.transports.File({ filename: "./logs/cps-extension-error.log", level: "error" }));
            logger.add(new winston.transports.File({ filename: "./logs/cps-extension.log" }));
        }
    }

    getLogger() {
        return logger;
    }
}

module.exports = logs;