/*
Helper module for CPS Exit REST service for Wacker-Neuson
establishing database connection to SAP Hana
providing globally used logging functions
Implemented by Matthias Plietz / 2022-02-02
changed 2022-02-09 new DB conn
*/

'use strict';

//-------------------------------------------------
//Function getSAPHanaConnection
//envelops the connection parameters to SAP Hana and connects to SAP Hana database
module.exports.getSAPHanaConnection = function(){

//  SAP Hana Connection
	var connOptions = {
//		serverNode: '7b03138c-5639-4e5c-8925-5bbcca6040a8.hana.trial-us10.hanacloud.ondemand.com:443',
//		UID: 'DBADMIN',
//		PWD: '!!4Jose2812',

		serverNode: "7a8ce924-5f92-45e7-bb44-d45b99c87cb1.hana.prod-eu10.hanacloud.ondemand.com:443",
		UID: "Z_CPQ_HDI_DB_1_BBJR0R020G4KJ8AINO9ZLJAGQ_RT",
		PWD: "Wf2_mj3bSHAf3xi0b.igXwFYBt4IvBJvAMOqeFCNmULZvFkadTVT-yttW1l.TKHvh8Qnx.b0.Lbu2y8R9u5K_6UFgSSG0i9eViC-NBm0sc7nmc5ibk_ssVwHbtxtzrga",

		traceFile: 'stdout',
		traceOptions: 'sql=warning',
		sslValidateCertificate: 'false'  //Must be set to false when connecting to an SAP HANA, express edition instance that uses a self-signed certificate.
	};
//  establish connection
	var hana = require('@sap/hana-client'); 
	var connection = hana.createConnection();
	connection.connect(connOptions);
	return connection; 
}


//-------------------------------------------------
//Function logging
//(first implementation phase)
module.exports.logging =function(ldata) {
	var util = require('util');
	//console.log(util.inspect(ldata, { colors: false }));
}


//-------------------------------------------------
//Funktion error logging
//(first implementation phase)
module.exports.error =function(ldata) {
	var util = require('util');
	console.log(util.inspect(ldata, { colors: true }));
}
