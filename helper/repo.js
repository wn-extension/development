'use strict';
var props = (new (require('./props.js')));
var hana = require('@sap/hana-client');
var util = require('util');
var schema="Z_CPQ_HDI_DB_1";
var connOptions = {
    host: "7a8ce924-5f92-45e7-bb44-d45b99c87cb1.hana.prod-eu10.hanacloud.ondemand.com",
    port: "443",
    UID: "Z_CPQ_HDI_DB_1_BBJR0R020G4KJ8AINO9ZLJAGQ_RT",
    PWD: "Wf2_mj3bSHAf3xi0b.igXwFYBt4IvBJvAMOqeFCNmULZvFkadTVT-yttW1l.TKHvh8Qnx.b0.Lbu2y8R9u5K_6UFgSSG0i9eViC-NBm0sc7nmc5ibk_ssVwHbtxtzrga",
    pooling: true,
    maxPoolSize: 15,
    connectionLifetime:120,
    prefetch: true,
    //Additional parameters
    //As of 2.7 trace info can be directed to stdout or stderr
    //traceFile: 'stdout',
    //traceOptions: 'sql=warning',

    //As of SAP HANA Client 2.6, connections on port 443 enable encryption by default (HANA Cloud).
    //encrypt: 'true',  //Must be set to true when connecting to HANA as a Service
    sslValidateCertificate: 'false',  //Must be set to false when connecting to an SAP HANA, express edition instance that uses a self-signed certificate.

    //For encrypted connections, the default crypto provider is mscrypto on Windows or openSSL on Linux or macos
    //To use the SAP crypto provider, uncomment the below line.
    //sslCryptoProvider: 'commoncrypto',

    //As of SAP HANA Client 2.6 for OpenSSL connections, the following settings can be ignored as root certificates are read from the default OS location.
    //ssltruststore: '/home/dan/.ssl/trust.pem', //Used to specify where the trust store is located
    //Alternatively provide the contents of the certificate directly (DigiCertGlobalRootCA.pem)
    //DigiCert Global Root CA: https://cacerts.digicert.com/DigiCertGlobalRootCA.crt.pem used for SAP HANA cloud
    //on-premise cert can be retrieved using openssl s_client -connect localhost:39015
    //This option is not supported with the mscrypto provider (the default provider on Windows)
    //ssltruststore: '-----BEGIN CERTIFICATE-----MIIDrzCCApegAwIBAgIQCDvgVpBCRrGhdWrJWZHHSjANBgkqhkiG9w0BAQUFADBhMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBDQTAeFw0wNjExMTAwMDAwMDBaFw0zMTExMTAwMDAwMDBaMGExCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jvhEXLeqKTTo1eqUKKPC3eQyaKl7hLOllsBCSDMAZOnTjC3U/dDxGkAV53ijSLdhwZAAIEJzs4bg7/fzTtxRuLWZscFs3YnFo97nh6Vfe63SKMI2tavegw5BmV/Sl0fvBf4q77uKNd0f3p4mVmFaG5cIzJLv07A6Fpt43C/dxC//AH2hdmoRBBYMql1GNXRor5H4idq9Joz+EkIYIvUX7Q6hL+hqkpMfT7PT19sdl6gSzeRntwi5m3OFBqOasv+zbMUZBfHWymeMr/y7vrTC0LUq7dBMtoM1O/4gdW7jVg/tRvoSSiicNoxBN33shbyTApOB6jtSj1etX+jkMOvJwIDAQABo2MwYTAOBgNVHQ8BAf8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUA95QNVbRTLtm8KPiGxvDl7I90VUwHwYDVR0jBBgwFoAUA95QNVbRTLtm8KPiGxvDl7I90VUwDQYJKoZIhvcNAQEFBQADggEBAMucN6pIExIK+t1EnE9SsPTfrgT1eXkIoyQY/EsrhMAtudXH/vTBH1jLuG2cenTnmCmrEbXjcKChzUyImZOMkXDiqw8cvpOp/2PV5Adg06O/nVsJ8dWO41P0jmP6P6fbtGbfYmbW0W5BjfIttep3Sp+dWOIrWcBAI+0tKIJFPnlUkiaY4IBIqDfv8NZ5YBberOgOzW6sRBc4L0na4UU+Krk2U886UAb3LujEV0lsYSEY1QSteDwsOoBrp+uvFRTp2InBuThs4pFsiv9kuXclVzDAGySj4dzp30d8tbQkCAUw7C29C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=-----END CERTIFICATE-----'
};



class repo {

    constructor () {
        connOptions.PWD = props.get('Repo.Password')
        connOptions.UID = props.get('Repo.User')
        connOptions.host = props.get('Repo.Host')
        connOptions.port = props.get('Repo.Port')
        connOptions.pooling=props.get('Repo.Pooling')
        schema=props.get('Repo.Schema');

    }

    
    traceOn() {
        if (props.get('CpsRepo.Trace') == 'true') {
            var traceCB = (buf) => {
                console.log(buf);
            };
            connection.onTrace("sql=error,debug=fatal,OutBufferSize=64k", traceCB);
        }
    }

    traceOff() {
        connection.onTrace("", null);
    }

    getConnection() {
      var client= hana.createClient();
      client.connect(connOptions);
      return client;
    }

    getInstance() {
        return CpsRepo.instance;
    }
    getSchema() {
        return schema.toString().length > 0 ? (schema.toString() + ".") : "";
    }
}
module.exports = repo;