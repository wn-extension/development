'use strict';
var strings = [];
class stringBuilder {
    constructor () {

    }

	append (string)
	{
		string = this.verify(string);
		if (string.length > 0) strings[strings.length] = string;
	};

	appendLine (string)
	{
		string = verify(string);
		if (this.isEmpty())
		{
			if (string.length > 0) strings[strings.length] = string;
			else return;
		}
		else strings[strings.length] = string.length > 0 ? "\r\n" + string : "\r\n";
	};

	clear () { strings = []; };

	isEmpty () { return strings.length == 0; };

	toString () { return strings.join(""); };

	verify(string) {
        if (!this.defined(string))
            return "";
        if (this.getType(string) != this.getType(new String()))
            return String(string);
        return string;
    }

	defined = function (el)
	{
		// Changed per Ryan O'Hara's comment:
		return el != null && typeof(el) != "undefined";
	};

	getType = function (instance)
	{
		if (!this.defined(instance.constructor)) throw Error("Unexpected object type");
		var type = String(instance.constructor).match(/function\s+(\w+)/);

		return this.defined(type) ? type[1] : "undefined";
	};
}
module.exports = stringBuilder;