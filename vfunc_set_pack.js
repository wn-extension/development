/*
Variant function ZVC_SET_PACK_2 for Wacker-Neuson
Adapted of ABAP function ZVC_SET_PACK_2 implemented by Peter Muthsam / SAP
Reimplemented in nodeJS by Ingo Beling / 2022-02-03
*/
'use strict';

//-------------------------------------------------
//Function ZVC_SET_PACK_2
//implementation of variant function ZVC_SET_ PACK_2 like similar ABAP function module
//remark: comments in German like corresponding code sections of ABAP
//

module.exports.ZVC_SET_PACK = function (fnArgs) {

	var result = true;
	var cstic = {};
	var value = "";
	var fnRes = [];

	//CONSTANTS: 	
	const lc_atnam_type = 'CGL_TYPE';
	const lc_atnam_model = 'CGL_MODEL';
	const lc_atnam_pack = 'CGL_PACK';
	const lc_atnam_pack_prefix = 'CGL_PACK';

	var v_CGL_TYPE = fnArgs.find(({ id }) => id === 'CGL_TYPE').values[0];
	var v_CGL_MODEL = fnArgs.find(({ id }) => id === 'CGL_MODEL').values[0];
	var v_CGL_PACK_01 = fnArgs.find(({ id }) => id === 'CGL_PACK_01').values[0];
	var v_CGL_PACK_02 = fnArgs.find(({ id }) => id === 'CGL_PACK_02').values[0];
	var v_CGL_PACK_03 = fnArgs.find(({ id }) => id === 'CGL_PACK_03').values[0];
	var v_CGL_PACK_04 = fnArgs.find(({ id }) => id === 'CGL_PACK_04').values[0];
	var v_CGL_PACK_05 = fnArgs.find(({ id }) => id === 'CGL_PACK_05').values[0];


	var lv_pack_counter = 1
	var lv_in_paras = ""
	if (v_CGL_PACK_01.length > 0) lv_in_paras = lv_in_paras + v_CGL_PACK_01 + ",";
	if (v_CGL_PACK_02.length > 0) lv_in_paras = lv_in_paras + v_CGL_PACK_02 + ",";
	if (v_CGL_PACK_03.length > 0) lv_in_paras = lv_in_paras + v_CGL_PACK_03 + ",";
	if (v_CGL_PACK_04.length > 0) lv_in_paras = lv_in_paras + v_CGL_PACK_04 + ",";
	if (v_CGL_PACK_05.length > 0) lv_in_paras = lv_in_paras + v_CGL_PACK_05 + ",";
	if (lv_in_paras.length > 0) lv_in_paras = lv_in_paras.substring(0, lv_in_paras.length - 1)


	var today = new Date().toISOString();

	var lt_pack_vals = [];
	var lt_vals = [];


	//  establish connection
	var repo = new (require('./helper/repo'));
	var connection = repo.getConnection();
	//    *******************************************

	//	  ********************************************
	//	  * Alle Werte für Paket auslesen
	//	  ********************************************

	var sql = "select * from " + repo.getSchema() + "ZVC_PACK where type = ? and ( model = ? or model = '*' ) and pack_id  in  (" + lv_in_paras + ") "
		+ " and datuv <= ? and datub > ?"
		+ " order by atnam";
	var params = [v_CGL_TYPE, v_CGL_MODEL, today, today];
	lt_vals = connection.exec(sql, params);
	connection.close()



	//	  * Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
	//	  * Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	var lt_pack_vals = removeDuplicates(lt_vals);

	//	  ********************************************
	//	  * Paket-werte setzen
	//	  ********************************************
	lt_pack_vals.forEach(function (row) {
		var cstic = { id: "id", type: "String", values: [] };
		cstic.id = row.ATNAM;
		value = "000" + row.ATWRT;
		value = value.substring(value.length - 3);  //numerischen Wert wieder mit leading zeros 
		cstic.values.push(value);
		fnRes.push(cstic);
		require('./helper/helper.js').logging(cstic);
	});


	function removeDuplicates(lt_vals) {
		let uniques = [];
		lt_vals.forEach(element => {
			uniques.push(element);
			uniques.forEach(compare => {
				if (compare.ATNAM == element.ATNAM) {
					if (Number(compare.ATWRT) > Number(element.ATWRT)) {
						uniques.splice(compare,1);
					}
				}
				
			})
			
		});
		return uniques;
	}
	return { status: 200, data: { type: "vfun", vfunOutput: { result: result, fnArgs: fnRes } } };
}
