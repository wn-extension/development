/*
Variant function ZVC_SET_DEFAULTS_2 for Wacker-Neuson
Adapted of ABAP function ZVC_SET_DEFAULTS_2 implemented by Peter Muthsam / SAP
Reimplemented in nodeJS by Matthias Plietz / 2022-02-03
2022-02-09 new DB access, cstic vals w/o parseInt, table access with schema, table access with date
*/
'use strict';

//-------------------------------------------------
//Function ZVC_SET_DEFAULTS_2
//implementation of variant function ZVC_SET_DEFAULTS_2 like similar ABAP function module
//remark: comments in German like corresponding code sections of ABAP
//
module.exports.ZVC_SET_DEFAULTS_2 = function (fnArgs) {
	const cstic_template = { id: "id", type: "String", values: [] };	
	const { PerformanceObserver, performance } = require('perf_hooks');
		
	var c = {};
	var value = "";
    var result = true;
	var fnRes = [];

	var v_CGL_TYPE     = fnArgs.find(({ id }) => id === 'CGL_TYPE').values[0];
	var v_CGL_MODEL    = fnArgs.find(({ id }) => id === 'CGL_MODEL').values[0];
	var v_CGL_VCREGION = fnArgs.find(({ id }) => id === 'CGL_VCREGION').values[0];
//    var v_CGL_VCREGION = (CGL_VCREGION == undefined) ? 0 : parseInt(CGL_VCREGION.values[0]);
	var v_CGL_COUNTRY  = fnArgs.find(({ id }) => id === 'CGL_COUNTRY').values[0];
	var v_CGL_SVERSION = fnArgs.find(({ id }) => id === 'CGL_SVERSION').values[0];	
    var today = new Date().toISOString();

	var lt_default_vals = [];
	var lt_vals = [];
	var lt_valsD = [];
	var lt_valsI = [];

//  establish connection
	var repo=new (require('./helper/repo'));
	var connection=repo.getConnection();
	
//****************************
//* Allg. Defaultwerte holen *
//****************************
//  Aus Basic-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen
    var sql = "select * from " + repo.getSchema() + "ZVC_BASIC_CFG where type = ? and ( model = ? or model = '*' ) and action = 'D'" 
	  + " and datuv <= ? and datub > ?"
	  + " order by atnam";
    var params = [ v_CGL_TYPE, v_CGL_MODEL, today, today ];
	lt_valsD = connection.exec(sql, params);
//  Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
//  Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	var lt_del = getDuplicates(lt_valsD, v_CGL_MODEL);

/*  Testcases:
	lt_del.push('CBS_BCKTPOSD');
	lt_del.push('CBS_LOADUNIT');
	lt_del.push('CCA_HVAC');
*/
//	Tabelleneinträge zu den Merkmalsnamen und * löschen, die zuvor ausgeschlossen wurden.
	lt_default_vals = lt_valsD.filter(f=> !lt_del.includes(f.ATNAM) || f.MODEL != '*')


//	*********************************************
//	* Spezifische Defaultwerte der Region holen *
//	*********************************************
//  Aus Region-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen
	sql = "select * from " + repo.getSchema() + "ZVC_REGION_CFG where type = ? and ( model = ? or model = '*' ) and region = ? "
	+ " and datuv <= ? and datub > ?"
	+ " order by atnam";
    params = [ v_CGL_TYPE, v_CGL_MODEL, v_CGL_VCREGION, today, today ];
	lt_vals = connection.exec(sql, params);
	lt_valsD = lt_vals.filter(f=> f.ACTION == 'D');
// 	Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
// 	Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	lt_del = getDuplicates(lt_valsD, v_CGL_MODEL);
//	Tabelleneinträge zu den Merkmalsnamen und * löschen, die zuvor ausgeschlossen wurden.
	var lt_region_vals = lt_valsD.filter(f=> !lt_del.includes(f.ATNAM) || f.MODEL != '*')
//  Update der Tabelle lt_default_vals mit den Region-spezifischen Defaultwerten
	lt_valsI = lt_vals.filter(f=> f.ACTION == 'I');
	lt_default_vals = specializeTable (lt_default_vals, lt_region_vals, lt_valsI);


//	*********************************************
//	* Spezifische Defaultwerte des Landes holen *
//	*********************************************
//  Aus Country-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen, für D und I
	sql = "select * from " + repo.getSchema() + "ZVC_COUNTRY_CFG where type = ? and ( model = ? or model = '*' ) and country = ? "
	+ " and datuv <= ? and datub > ?"
	+ " order by atnam";
    params = [ v_CGL_TYPE, v_CGL_MODEL, v_CGL_COUNTRY, today, today ];
	lt_vals = connection.exec(sql, params);
	lt_valsD = lt_vals.filter(f=> f.ACTION == 'D');
// 	Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
// 	Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	lt_del = getDuplicates(lt_valsD, v_CGL_MODEL);
//	Tabelleneinträge zu den Merkmalsnamen und * löschen, die zuvor ausgeschlossen wurden.
	var lt_country_vals = lt_valsD.filter(f=> !lt_del.includes(f.ATNAM) || f.MODEL != '*')
//  Update der Tabelle lt_default_vals mit den Country-spezifischen Defaultwerten
	lt_valsI = lt_vals.filter(f=> f.ACTION == 'I');
	lt_default_vals = specializeTable (lt_default_vals, lt_country_vals, lt_valsI);


//	*******************************************************
//	* Spezifische Defaultwerte der Sonderausführung holen *
//	*******************************************************
//  Aus Country-Tabelle alle Einträge für das spezifische Modell und mit Modell = * holen, für D und I
	sql = "select * from " + repo.getSchema() + "ZVC_SPECIAL_CFG where type = ? and ( model = ? or model = '*' ) and sversion = ? "
	+ " and datuv <= ? and datub > ?"
	+ " order by atnam";
    params = [ v_CGL_TYPE, v_CGL_MODEL, v_CGL_SVERSION, today, today ];
	lt_vals = connection.exec(sql, params);
	lt_valsD = lt_vals.filter(f=> f.ACTION == 'D');
	// 	Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
	// 	Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	lt_del = getDuplicates(lt_valsD, v_CGL_MODEL);
	//	Tabelleneinträge zu den Merkmalsnamen und * löschen, die zuvor ausgeschlossen wurden.
	var lt_special_vals = lt_valsD.filter(f=> !lt_del.includes(f.ATNAM) || f.MODEL != '*')
	//  Update der Tabelle lt_default_vals mit den Sonder-Defaultwerten
	lt_valsI = lt_vals.filter(f=> f.ACTION == 'I');
	lt_default_vals = specializeTable (lt_default_vals, lt_special_vals, lt_valsI);


	connection.close();
//	********************************************
//	* Defaultwerte setzen
//	********************************************

	lt_default_vals.forEach(function(row){
		var cstic = { id: "id", type: "String", values: [] };
		cstic.id = row.ATNAM;
		value = "000" + row.ATWRT;
		value = value.substring(value.length-3);  //numerischen Wert wieder mit leading zeros 
		cstic.values.push (value);
		fnRes.push (cstic);
		logging(cstic);
	});
	
	var result = { status: 200, data: { type: "vfun", vfunOutput: { result: result, fnArgs: fnRes } } };
    return result;
}



//-------------------------------------------------
//Function getDuplicates
//Selection of cstic names assigned to both the concrete model and its widecard *
//List is to delete the * table entries in next processing step
//there are several table entries possible per ATNAM and to both the concrete model and its widecard *
//remarks: in difference to ABAP this function is extracted and refactored for several calls, 
//         and the comments are in German like corresponding code sections of ABAP
//
function getDuplicates(lt_vals, lv_CGL_MODEL){
// Jetzt gruppenweise Betrachtung der internen Tabelle: Gruppierung nach ATNAM (Merkmalsname definiert die Gruppe)
// Pro Gruppe (ATNAM) kann es 1 Eintrag (mit spez. Modell) oder 2 Einträge (mit spez. Modell und *) geben.
	var c_prevATNAM = '';
	var lt_del = [];
	lt_vals.forEach(function(row){
		if (row.ATNAM != c_prevATNAM)
		{
//          Gruppenwechsel:
			c_prevATNAM = row.ATNAM;
// 			Hilfstabelle lt_vals_2 aufbauen; diese enthält die Einträge für das aktuelle ATNAM
			var lt_vals_2 = [];
			lt_vals.filter(f=>f.ATNAM==row.ATNAM).forEach(function(row) {
				lt_vals_2.push(row);
			});
//		    Enthält die Tabelle mindestens ein Modell und mindestens einen Sterneintrag?:
			if (lt_vals_2.filter(f=>f.MODEL=='*').length>0) {
				if (lt_vals_2.filter(f=>f.MODEL==lv_CGL_MODEL).length>0) {
					lt_del.push(row.ATNAM)
				}
			}
		}
	});
	return lt_del;
}



//-------------------------------------------------
//Function specializeTable
//Update of the more general table of default values of the previous step with the specific table 
//of this processing step
//remarks: in difference to ABAP this function is extracted and refactored for several calls, 
//         and the comments are in German like corresponding code sections of ABAP
//
function specializeTable (it_defaultsTable, it_newDefaultsTable, it_newInsertsTable){
//	Update der Tabelle lt_default_vals mit den country-spezifischen Defaultwerten
//  über alle Einträge: neu-->anlegen, vorhanden-->update
	it_newDefaultsTable.forEach(function(row){
		var it_cstics = it_defaultsTable.filter(f=>f.ATNAM==row.ATNAM);
		if (it_cstics.length == 0){
//			neuer cstic Eintrag: einfügen
			var row2 = {};
			row2.TYPE = row.TYPE;
			row2.MODEL = row.MODEL;
			row2.ATNAM = row.ATNAM;
			row2.ATWRT = row.ATWRT;
			row2.ACTION = row.ACTION;
			it_defaultsTable.push(row2);
		}
		else {
//			vorhandener cstic Eintrag: updaten
			it_cstics.forEach(function(cstic){
				cstic.ATWRT = row.ATWRT;
			});	
		}
	});

// Defaultwerte, die aus einer der vorangegangenen, allgemeineren Tabellen geholt wurden,
// müssen gelöscht werden, falls diese spezielle Tabelle zwar Include-Einträge, aber keinen Default-Eintrag für das Merkmal hat
/*
LOOP AT lt_default_vals ASSIGNING <fs_default_vals>.
* Gibt es Include-Einträge?
    READ TABLE lt_country_cfg TRANSPORTING NO FIELDS 
		WITH KEY atnam = <fs_default_vals>-atnam action = 'I'.
    IF sy-subrc = 0.
* Gab es einen Default-Eintrag?
      READ TABLE lt_country_cfg TRANSPORTING NO FIELDS
        WITH KEY atnam = <fs_default_vals>-atnam action = 'D'.
* Nein -> dann müssen wir den Eintrag aus der lt_defaults_val löschen
      IF sy-subrc <> 0.
        DELETE lt_default_vals WHERE atnam = <fs_default_vals>-atnam.
      ENDIF.  " Kein Default-Eintrag vorhanden
    ENDIF.    " Include-Einträge vorhanden
  ENDLOOP.    " AT lt_default_vals
*/

	it_defaultsTable.forEach(function(row){
//		Gibt es Include-Einträge?
		var i_entriesI = it_newInsertsTable.filter(f=> f.ATNAM == row.ATNAM).length;
		if (i_entriesI > 0) {
//			Gab es einen Default-Eintrag?
			var i_entriesD = it_newDefaultsTable.filter(f=> f.ATNAM == row.ATNAM).length;
			if (i_entriesD == 0) {
//				Nein -> dann müssen wir den Eintrag aus der lt_defaults_val löschen
//				DELETE lt_default_vals WHERE atnam = <fs_default_vals>-atnam.
				row.ACTION = 'X';  //Marker setzen
			}
		}
	});
//  markierten Eintrag X löschen, um nicht innerhalb der foreach Schleife Einträge zu löschen
	it_defaultsTable = it_defaultsTable.filter(f=> f.ACTION != 'X');

	return it_defaultsTable;
}



//-------------------------------------------------
//Function logging
//called in helper module
//
function logging (ldata) {
	require('./helper/helper.js').logging (ldata);
}
