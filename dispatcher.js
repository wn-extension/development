/*
CPS Exit REST service for Wacker-Neuson
providing dispacher functions for variant functions and pricing exits of SAP CPS 
Implemented by Matthias Plietz / 2022-02-03
changed 2022-02-09
*/
'use strict';

//-------------------------------------------------
//Function functionDispatcher
//receives and dispaches the variant function calls to the processing functions
module.exports.functionDispatcher = function(data) {
	var result = { status: 501, data: { type: "vfun", vfunOutput: { result: false } } };
	var vfunction = null;
    var vfunc = data.vfunInput

	if (typeof vfunc === 'undefined')
		return result
	
    switch (vfunc.id) {
        case "ZVC_SET_DEFAULTS_2":	
			vfunction = require('./vfunc_set_default.js');
			result = vfunction.ZVC_SET_DEFAULTS_2 (vfunc.fnArgs)
			break

        case "ZVC_SET_EDITION_2":	
			vfunction = require('./vfunc_set_edition.js');
			result = vfunction.ZVC_SET_EDITION_2 (vfunc.fnArgs)
			break

        case "ZVC_SET_PACK_2":	
			vfunction = require('./vfunc_set_pack.js');
			result = vfunction.ZVC_SET_PACK (vfunc.fnArgs)
			break

	}
	return result;
}	


//-------------------------------------------------
//Function formulaDispatcher
//receives and dispaches the pricing exit calls to the processing functions
module.exports.formulaDispatcher = function(data) {
	var formula = require('./formulas.js');
    var action = data.action
	var result = { status: 400, data: { result: false, message: "wrong action " + action } };
    switch (action) {
        case "PROCESS_FORMULA":	
			switch (data.formulaNumber) {
				case 904: 
					result = formula.processFormula_REQ904 (data)
					break
				case 957: 
					result = formula.processFormula_BAS957 (data)
					break
				default:
					result = { status: 501, data: { result: false, message: "missing formula " + data.formulaType + "_" + data.formulaNumber } };
/*
				known formulas
					formulaNumber: 803, formulaType: VAL (PNTP step 303)
					formulaNumber: 802, formulaType: BAS (ZW51 step 929)
					formulaNumber: 804, formulaType: VAL (step 950)
					formulaNumber: 913, formulaType: REQ
*/
			}
			break
			
        case "COLLECT_ATTRIBUTES":	
			result = formula.getAttributes (data)
			break
	}
	return result;
}	

