'use strict';

// action == PROCESS_FORMULA
module.exports.processFormula_REQ904 = function (data) {
	var status = 200
    var result = true;
    var message = data.formulaType + '_' + data.formulaNumber + ' processed';

	var response = { status: status, data: { result: result, message: message } };
    return response;
}



module.exports.processFormula_BAS957 = function (data) {
// formulaNumber: 957, formulaType: BAS (ZW01 step 50) Subtraktion von ZVAN
//   lt_xkomv[] = xkomv[].
//   LOOP AT lt_xkomv INTO DATA(ls_xkomv)
//     WHERE kposn = komp-kposn
//       AND kschl = 'ZVAN'
//       AND kinak = ''. "active
//     xkwert      = xkwert      - ls_xkomv-kbetr.
    return { status: 501, data: { result: false, message: "formula to be coded " + data.formulaType + "_" + data.formulaNumber } };
}




// action == COLLECT_ATTRIBUTES, hier nicht unterschieden für die Formulas 
module.exports.getAttributes = function (data){
    var attributeList = ["KOMK-WAERK"];		//sample
	response = { result: attributeList, message: "" };
    return response;
}
